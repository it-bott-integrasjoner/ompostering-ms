import logging
from typing import List

import ompostering_client.models as om_models
import ubw_client.models as ubw_models

from ompostering_ms.context import OmposteringMsContext

logger = logging.getLogger(__name__)


def fetch_from_ubw(
    ctx: OmposteringMsContext,
    company_ids: List[str],
    ignore_errors: bool,
) -> List[ubw_models.Begrepsverdi]:
    koststeder: List[ubw_models.Begrepsverdi] = []
    for company_id in company_ids:
        try:
            logger.info("Fetching koststeder with company_id=%s", company_id)
            koststed_set = ctx.ubw.get_koststed(company_id)
            if koststed_set:
                for anlegg in koststed_set:
                    koststeder.append(anlegg)  # type: ignore[arg-type]
        except Exception as exc:
            logger.error(
                "Failed to fetch koststeder with company_id=%s",
                company_id,
                exc_info=exc,
            )
            if not ignore_errors:
                raise exc
    return koststeder


# TODO: Refaktorer duplisert kode provision/*.py (se for eksempel anlegg.py)
def convert_ubw_to_ompostering(
    instances: List[ubw_models.Begrepsverdi],
    ignore_errors: bool,
) -> List[om_models.Begrepsverdi]:
    begrepsverdier_om: List[om_models.Begrepsverdi] = []
    for begrepsverdi_ubw in instances:
        try:
            begrepsverdier_om.append(om_models.Begrepsverdi(**begrepsverdi_ubw.dict()))
        except Exception as exc:
            logger.error(
                "Failed to convert begrepsverdi with company_id=%s attribute_id=%s attribute_value=%s",
                begrepsverdi_ubw.company_id,
                begrepsverdi_ubw.attribute_id,
                begrepsverdi_ubw.attribute_value,
                exc_info=exc,
            )
            if not ignore_errors:
                raise exc
    return begrepsverdier_om


def post_to_ompostering(
    ctx: OmposteringMsContext,
    instances: List[om_models.Begrepsverdi],
    ignore_errors: bool,
):
    for i, begrepsverdi in enumerate(instances):
        # TODO: This loop is a potential candidate for parallelization
        logger.info(
            "Posting begrepsverdi with company_id=%s attribute_id=%s attribute_value=%s (%s/%s)...",
            begrepsverdi.company_id,
            begrepsverdi.attribute_id,
            begrepsverdi.attribute_value,
            i + 1,
            len(instances),
        )
        try:
            ctx.ompostering.post_begrepsverdi(begrepsverdi)
        except Exception as exc:
            logger.error(
                "Failed to post begrepsverdi with company_id=%s attribute_id=%s attribute_value=%s",
                begrepsverdi.company_id,
                begrepsverdi.attribute_id,
                begrepsverdi.attribute_value,
                exc_info=exc,
            )
            if not ignore_errors:
                raise exc
