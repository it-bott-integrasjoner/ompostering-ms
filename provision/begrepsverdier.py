import logging
from typing import List, Sequence

import ompostering_client.models as om_models
import ubw_client.models as ubw_models

from ompostering_ms.context import OmposteringMsContext
from ompostering_ms.handlers import PROVISIONED_ATTRIBUTE_IDS

logger = logging.getLogger(__name__)


def fetch_from_ubw(
    ctx: OmposteringMsContext,
    company_ids: List[str],
    ignore_errors: bool,
) -> Sequence[ubw_models.BaseModel]:
    begrepsverdier: List[ubw_models.Begrepsverdi] = []
    for company_id in company_ids:
        for attribute_id in PROVISIONED_ATTRIBUTE_IDS:
            try:
                logger.info(
                    "Fetching begrepsverdier with company_id=%s attribute_id=%s...",
                    company_id,
                    attribute_id,
                )
                begrepsverdi = ctx.ubw.get_begrepsverdier(
                    company_id,
                    begrep_id=attribute_id,
                    aktiv=False,
                )
                if begrepsverdi:
                    begrepsverdier += begrepsverdi  # type: ignore[arg-type]
                else:
                    logger.warning("Ingen begrepsverdi funnet for id %s", attribute_id)
            except Exception as exc:
                logger.error(
                    "Failed to fetch begrepsverdier with company_id=%s attribute_id=%s",
                    company_id,
                    attribute_id,
                    exc_info=exc,
                )
                if not ignore_errors:
                    raise exc
    return begrepsverdier


def convert_ubw_to_ompostering(
    instances: Sequence[ubw_models.Begrepsverdi],
    ignore_errors: bool,
) -> Sequence[om_models.BaseModel]:
    begrepsverdier_om: List[om_models.Begrepsverdi] = []
    for begrepsverdi_ubw in instances:
        try:
            begrepsverdier_om.append(om_models.Begrepsverdi(**begrepsverdi_ubw.dict()))
        except Exception as exc:
            logger.error(
                "Failed to convert begrepsverdi with company_id=%s attribute_id=%s attribute_value=%s",
                begrepsverdi_ubw.company_id,
                begrepsverdi_ubw.attribute_id,
                begrepsverdi_ubw.attribute_value,
                exc_info=exc,
            )
            if not ignore_errors:
                raise exc
    return begrepsverdier_om


def post_to_ompostering(
    ctx: OmposteringMsContext,
    instances: Sequence[om_models.Begrepsverdi],
    ignore_errors: bool,
):
    try:
        ctx.ompostering.post_begrepsverdier_batch(instances)
    except Exception as exc:
        logger.error("Failed to post begrepsverdier batch", exc_info=exc)
        if not ignore_errors:
            raise exc
