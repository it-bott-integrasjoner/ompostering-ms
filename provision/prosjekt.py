import logging
from typing import List, Sequence

import ompostering_client.models as om_models
import ubw_client.models as ubw_models

from ompostering_ms.context import OmposteringMsContext

logger = logging.getLogger(__name__)


def fetch_from_ubw(
    ctx: OmposteringMsContext,
    company_ids: List[str],
    ignore_errors: bool,
) -> Sequence[ubw_models.Prosjekt]:
    projects: Sequence[ubw_models.Prosjekt] = []
    for i, company_id in enumerate(company_ids):
        try:
            logger.info(
                "Fetching prosjekt with company_id=%s (%s/%s)...",
                company_id,
                i + 1,
                len(company_ids),
            )
            projects += ctx.ubw.get_prosjekter(company_id)  # type: ignore[operator]
        except Exception as exc:
            logger.error(
                "Failed to fetch prosjekter with company_id=%s",
                company_id,
                exc_info=exc,
            )
            if not ignore_errors:
                raise exc
    return projects


def convert_ubw_to_ompostering(
    instances: Sequence[ubw_models.Prosjekt],
    ignore_errors: bool,
) -> Sequence[om_models.Prosjekt]:
    prosjekter_om: List[om_models.Prosjekt] = []
    for prosjekt_ubw in instances:
        try:
            prosjekter_om.append(om_models.Prosjekt(**prosjekt_ubw.dict()))
        except Exception as exc:
            logger.error(
                "Failed to convert prosjekt with ID=%s",
                prosjekt_ubw.project_id,
                exc_info=exc,
            )
            if not ignore_errors:
                raise exc
    return prosjekter_om


def post_to_ompostering(
    ctx: OmposteringMsContext,
    instances: Sequence[om_models.Prosjekt],
    ignore_errors: bool,
):
    project_ids = [project.project_id for project in instances]
    logger.info(
        "Posting prosjekter with prosjekt_ids: %s, nr of prosjekt: %s)...",
        project_ids,
        len(instances),
    )
    if not instances:
        logger.info("Empty projects, nothing to provision")
        return
    try:
        ctx.ompostering.post_prosjekt_batch(instances)
    except Exception as exc:
        logger.error(
            "Failed to post prosjekter with prosjekt_ids=%s",
            project_ids,
            exc_info=exc,
        )
        if not ignore_errors:
            raise exc
