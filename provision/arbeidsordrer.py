import logging
from typing import List, Sequence

import ompostering_client.models as om_models
import ubw_client.models as ubw_models

from ompostering_ms.context import OmposteringMsContext
from ompostering_ms.handlers import arbeidsordre_ubw_to_om

logger = logging.getLogger(__name__)


def fetch_from_ubw(
    ctx: OmposteringMsContext,
    company_ids: List[str],
    ignore_errors: bool,
) -> Sequence[ubw_models.Arbeidsordre]:
    arbeidsordrer: List[ubw_models.Arbeidsordre] = []
    for i, company_id in enumerate(company_ids):
        try:
            logger.info(
                "Fetching arbeidsordrer with company_id=%s (%s/%s)...",
                company_id,
                i + 1,
                len(company_ids),
            )
            arbeidsordrer += ctx.ubw.get_partial_arbeidsordrer(company_id)  # type: ignore[arg-type]
        except Exception as exc:
            logger.error(
                "Failed to fetch arbeidsordrer with company_id=%s",
                company_id,
                exc_info=exc,
            )
            if not ignore_errors:
                raise exc
    return arbeidsordrer


def convert_ubw_to_ompostering(
    instances: Sequence[ubw_models.Arbeidsordre],
    ignore_errors: bool,
) -> Sequence[om_models.Arbeidsordre]:
    arbeidsordrer_om: List[om_models.Arbeidsordre] = []
    for arbeidsordre_ubw in instances:
        try:
            arbeidsordrer_om.append(arbeidsordre_ubw_to_om(arbeidsordre_ubw))
        except Exception as exc:
            logger.error(
                "Failed to convert arbeidsordre with ID=%s",
                arbeidsordre_ubw.work_order_id,
                exc_info=exc,
            )
            if not ignore_errors:
                raise exc
    return arbeidsordrer_om


def post_to_ompostering(
    ctx: OmposteringMsContext,
    instances: Sequence[om_models.Arbeidsordre],
    ignore_errors: bool,
):
    logger.info("Posting %s arbeidsordre", len(instances))
    try:
        ctx.ompostering.post_arbeidsordre_batch(instances)
    except Exception as e:
        logger.error("Failed to post arbeidsordre batch", exc_info=e)
        if not ignore_errors:
            raise e
