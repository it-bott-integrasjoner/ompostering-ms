import logging
from typing import List, Sequence

import ompostering_client.models as om_models
import ubw_client.models as ubw_models
from ompostering_ms.context import OmposteringMsContext


logger = logging.getLogger(__name__)


def fetch_from_ubw(
    ctx: OmposteringMsContext,
    company_ids: List[str],
    ignore_errors: bool,
) -> Sequence[ubw_models.Avgiftskode]:
    avgiftskoder: List[ubw_models.Avgiftskode] = []
    for i, company_id in enumerate(company_ids):
        try:
            logger.info(
                "Fetching avgiftskoder with company_id=%s (%s/%s)...",
                company_id,
                i + 1,
                len(company_ids),
            )
            avgiftskoder.append(ctx.ubw.get_avgiftskode(company_id))  # type: ignore[arg-type]
        except Exception as exc:
            logger.error(
                "Failed to fetch avgiftskoder with company_id=%s",
                company_id,
                exc_info=exc,
            )
            if not ignore_errors:
                raise exc
    return avgiftskoder


def convert_ubw_to_ompostering(
    instances: Sequence[Sequence[ubw_models.Avgiftskode]],
    ignore_errors: bool,
) -> Sequence[om_models.BaseModel]:
    om_avgiftskoder: List[om_models.Avgiftskoder] = []
    for ubw_avgiftskoder in instances:
        company_id: str = ubw_avgiftskoder[0].company_id
        try:
            avgiftskoder = [om_models.Avgiftskode(**x.dict()) for x in ubw_avgiftskoder]
            om_avgiftskoder.append(
                om_models.Avgiftskoder(company_id=company_id, avgiftskoder=avgiftskoder)
            )
        except Exception as exc:
            logger.error(
                "Failed to convert avgiftskoder with company_id=%s",
                company_id,
                exc_info=exc,
            )
            if not ignore_errors:
                raise exc
    return om_avgiftskoder


def post_to_ompostering(
    ctx: OmposteringMsContext,
    instances: Sequence[om_models.Avgiftskoder],
    ignore_errors: bool,
) -> None:
    for i, avgiftskoder in enumerate(instances):
        # TODO: This loop is a potential candidate for parallelization
        logger.info(
            "Posting avgiftskoder with company_id=%s (%s/%s)...",
            avgiftskoder.company_id,
            i + 1,
            len(instances),
        )
        try:
            ctx.ompostering.post_avgiftskoder(
                avgiftskoder.company_id, avgiftskoder.avgiftskoder
            )
        except Exception as exc:
            logger.error("Failed to post avgiftskoder with company_id=%s", exc_info=exc)
            if not ignore_errors:
                raise exc
