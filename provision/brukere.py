from collections import defaultdict

import datetime
import logging
from typing import List, Sequence, Tuple

import ompostering_client.models as om_models
import ubw_client.models as ubw_models

from ompostering_ms.context import OmposteringMsContext

logger = logging.getLogger(__name__)


def fetch_from_ubw(
    ctx: OmposteringMsContext,
    company_ids: List[str],
    ignore_errors: bool,
) -> Sequence[Tuple[str, ubw_models.Bruker]]:
    brukere = []
    now: datetime.datetime = datetime.datetime.now()
    for i, company_id in enumerate(company_ids):
        company_brukere: List[ubw_models.Bruker] = []
        try:
            logger.info(
                "Fetching brukere with company_id=%s (%s/%s)...",
                company_id,
                i + 1,
                len(company_ids),
            )
            company_brukere = ctx.ubw.get_brukere(company_id)  # type: ignore[assignment]
        except Exception as exc:
            logger.error(
                "Failed to fetch brukere with company_id=%s", company_id, exc_info=exc
            )
            if not ignore_errors:
                raise exc

        # Filtering
        for bruker in company_brukere:
            if not bruker.user_status:
                logger.info("Bruker %s is missing userStatus, skipping", bruker.user_id)
                continue
            date_from = bruker.user_status.date_from
            date_to = bruker.user_status.date_to or datetime.datetime.max
            is_active = bruker.user_status.status == "N" and date_from <= now <= date_to

            has_active_relevant_role = False
            if is_active and bruker.role_and_company:
                for role in bruker.role_and_company:
                    if (
                        role.company_id == company_id
                        and role.role_connection_status == "N"
                        and role.role_connection_valid_from
                        <= now
                        <= role.role_connection_valid_to
                    ):
                        has_active_relevant_role = True
                        break

            if is_active and has_active_relevant_role:
                brukere.append((company_id, bruker))

    return brukere


def ubw_role_and_company_to_om_role_and_company(
    role_and_company: ubw_models.RoleAndCompany,
) -> om_models.RoleAndCompany:
    return om_models.RoleAndCompany(
        role_id=role_and_company.role_id,
        company_id=role_and_company.company_id,
        person_id=role_and_company.person_id,
        resourceConnectionUpdated=role_and_company.resource_connection_updated,
        resourceConnectionUpdatedBy=role_and_company.resource_connection_updated_by,
        role_connection_valid_from=role_and_company.role_connection_valid_from,
        role_connection_valid_to=role_and_company.role_connection_valid_to,
        role_connection_updated_at=role_and_company.role_connection_updated_at,
        role_connection_status=role_and_company.role_connection_status,
        role_connection_updated_by=role_and_company.role_connection_updated_by,
    )


def ubw_user_status_to_om_user_status(
    user_status: ubw_models.UserStatus,
) -> om_models.UserStatus:
    return om_models.UserStatus(
        date_from=user_status.date_from,
        date_to=user_status.date_to,
        status=user_status.status,
    )


def ubw_security_to_om_security(
    user_status: ubw_models.Security,
) -> om_models.Security:
    return om_models.Security(disabled_until=user_status.disabled_until)


def ubw_usage_to_om_usage(
    usage: ubw_models.Usage,
) -> om_models.Usage:
    return om_models.Usage(
        is_enabled_for_workflow_process=usage.is_enabled_for_workflow_process
    )


def ubw_contact_point_to_om_contact_point(
    contact_point: ubw_models.ContactPoint,
) -> om_models.ContactPoint:
    return om_models.ContactPoint(
        additional_contact_info=contact_point.additional_contact_info,
        address=contact_point.address,
        contact_point_type=contact_point.contact_point_type,
        last_updated=contact_point.last_updated,
        phone_numbers=contact_point.phone_numbers,
        sort_order=contact_point.sort_order,
    )


def ubw_bruker_to_ompostering_bruker(bruker_ubw: ubw_models.Bruker) -> om_models.Bruker:
    roles_and_companies = (
        None
        if bruker_ubw.role_and_company is None
        else [
            ubw_role_and_company_to_om_role_and_company(x)
            for x in bruker_ubw.role_and_company
        ]
    )
    contact_points = (
        None
        if bruker_ubw.contact_points is None
        else [
            ubw_contact_point_to_om_contact_point(x) for x in bruker_ubw.contact_points
        ]
    )
    user_status = (
        None
        if bruker_ubw.user_status is None
        else ubw_user_status_to_om_user_status(bruker_ubw.user_status)
    )
    security = (
        None
        if bruker_ubw.security is None
        else ubw_security_to_om_security(bruker_ubw.security)
    )
    usage = (
        None if bruker_ubw.usage is None else ubw_usage_to_om_usage(bruker_ubw.usage)
    )
    return om_models.Bruker(
        user_id=bruker_ubw.user_id,
        user_name=bruker_ubw.user_name,
        description=bruker_ubw.description,
        default_logon_company=bruker_ubw.default_logon_company,
        user_status=user_status,
        security=security,
        language_code=bruker_ubw.language_code,
        usage=usage,
        contact_points=contact_points,
        roles_and_companies=roles_and_companies,
    )


def convert_ubw_to_ompostering(
    instances: Sequence[Tuple[str, ubw_models.Bruker]],
    ignore_errors: bool,
) -> Sequence[Tuple[str, om_models.Bruker]]:
    brukere_om = []
    for company_id, bruker_ubw in instances:
        try:
            brukere_om.append(
                (company_id, ubw_bruker_to_ompostering_bruker(bruker_ubw))
            )
        except Exception as exc:
            logger.error(
                "Failed to convert bruker with ID=%s", bruker_ubw.user_id, exc_info=exc
            )
            if not ignore_errors:
                raise exc
    return brukere_om


def post_to_ompostering(
    ctx: OmposteringMsContext,
    instances: Sequence[Tuple[str, om_models.Bruker]],
    ignore_errors: bool,
):
    if not instances:
        return

    firma_brukere = defaultdict(list)
    for item in instances:
        firma_brukere[item[0]].append(item[1])

    for company_id, brukere in firma_brukere.items():
        try:
            logger.info(
                "Posting brukere with company_id=%s, antall brukere: %s...",
                company_id,
                len(brukere),
            )
            ctx.ompostering.post_brukere_batch(company_id, brukere)
        except Exception as exc:
            logger.error(
                "Failed to post brukere (%s) with company_id=%s",
                len(brukere),
                company_id,
                exc_info=exc,
            )
            if not ignore_errors:
                raise exc
