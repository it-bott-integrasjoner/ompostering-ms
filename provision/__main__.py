"""Command line tool for provisioning complete sets of attributes from UBW to
Omposteringsløsningen.

Invoke with `python -m provision`. Add `-h` to see usage and options.

Apart from CLI options, the tool is configured using a config file identical to
the one used by ompostering-ms.

Provisioning is a three-step process. For each attribute, we must

1. Fetch the attribute instances from UBW.
2. Convert the instances for ompostering.
3. Post the instances to ompostering.

This tool carries out one step for all specified attributes before proceeding
to the next step. All POSTing is thus done only at the end when everything else
has succeeded. This should reduce the risk of having errors cause partially
completed provisionings.

To add a new attribute:

1. Copy and modify one of the existing attribute modules (e.g. perioder.py).
2. Import it here.
3. Add the module to ATTRIBUTE_MODULES.
4. Write tests.
"""

import argparse
import concurrent.futures
import logging
import os
import sys
import types
import typing

import ompostering_client.models as om_models
import pydantic
import ubw_client.models as ubw_models

from ompostering_ms.config import get_config_loader
from ompostering_ms.context import OmposteringMsContext
from ompostering_ms.utils import setup_logging
from provision import (
    anlegg,
    arbeidsordrer,
    avgiftskoder,
    begrepsverdier,
    brukere,
    firma,
    konteringsregler,
    kontoplan,
    koststeder,
    perioder,
    prosjekt,
)

logger = logging.getLogger(__name__)


# The keys of this dict determine the CLI arg names used for specifying
# attributes for provisioning. The values are Python modules used for
# processing each attribute.
ATTRIBUTE_MODULES: typing.Dict[str, types.ModuleType] = {
    "anlegg": anlegg,
    "arbeidsordrer": arbeidsordrer,
    "begrepsverdier": begrepsverdier,
    "brukere": brukere,
    "firma": firma,
    "kontoplan": kontoplan,
    "koststeder": koststeder,
    "perioder": perioder,
    "prosjekt": prosjekt,
    "konteringsregler": konteringsregler,
    "avgiftskoder": avgiftskoder,
}


class ExitCode:
    OK = 0
    NO_CONFIG_FILE = 1
    NO_ATTRIBUTES = 2
    USER_ABORT = 3


def _is_yes(prompt_answer: str):
    return prompt_answer.lower().strip() in ("y", "yes")


def _fetch_from_ubw(
    ctx: OmposteringMsContext,
    company_ids: typing.List[str],
    attribute: str,
    ignore_errors: bool,
) -> typing.Dict[str, typing.Sequence[ubw_models.BaseModel]]:
    instances: typing.Dict[str, typing.Sequence[ubw_models.BaseModel]] = {}
    attribute_module = ATTRIBUTE_MODULES[attribute]
    logger.info("Fetching %s from UBW...", attribute)
    # Call `fetch_from_ubw()` of the appropriate module
    instances[attribute] = attribute_module.fetch_from_ubw(
        ctx, company_ids, ignore_errors
    )
    logger.info("Fetched %s %s.", len(instances[attribute]), attribute)
    return instances


def _convert_ubw_to_ompostering(
    instances_ubw: typing.Dict[str, typing.Sequence[ubw_models.BaseModel]],
    attribute: str,
    ignore_errors: bool,
) -> typing.Dict[str, typing.Sequence[om_models.BaseModel]]:
    instances_ompostering: typing.Dict[str, typing.Sequence[om_models.BaseModel]] = {}

    # We assume that processing for different attributes happens separately
    # in parallel.
    attribute_module = ATTRIBUTE_MODULES[attribute]
    for _, attribute_instances in instances_ubw.items():
        logger.info("Converting %s for Ompostering...", attribute)
        # Call `convert_ubw_to_ompostering()` of the appropriate module
        instances_ompostering[attribute] = attribute_module.convert_ubw_to_ompostering(
            attribute_instances, ignore_errors
        )
    return instances_ompostering


def _post_to_ompostering(
    ctx: OmposteringMsContext,
    instances: typing.Dict[str, typing.Sequence[om_models.BaseModel]],
    attribute: str,
    ignore_errors: bool,
):
    # We assume that this method accepts a list of items with the same attribute
    attribute_module = ATTRIBUTE_MODULES[attribute]
    for item_attribute, attribute_instances in instances.items():
        if item_attribute == attribute:
            logger.info("Posting %s to Ompostering...", attribute)
            # Call `post_to_ompostering()` of the appropriate module
            attribute_module.post_to_ompostering(
                ctx, attribute_instances, ignore_errors
            )


def prepare_for_posting(ctx, args, attribute):
    instances_ubw = _fetch_from_ubw(ctx, args.company_id, attribute, args.ignore_errors)
    instances_ompostering = _convert_ubw_to_ompostering(
        instances_ubw, attribute, args.ignore_errors
    )
    return instances_ompostering


def log_provision_info(
    instances_ompostering: typing.List[
        typing.Dict[str, typing.Sequence[om_models.BaseModel]]
    ]
):
    message = "\nReady to post the following to Ompostering:"
    for instances_list in instances_ompostering:
        for attribute, instances in instances_list.items():
            if isinstance(instances, typing.Sequence):
                message += f"\t{attribute}: {len(instances)}"
            elif isinstance(instances, pydantic.BaseModel) and hasattr(
                instances, "__root__"
            ):
                message += f"\t{attribute}: {len(instances.__root__)}"
            elif isinstance(instances, dict):
                for k, v in instances.items():
                    message += f"\t{attribute} (firm {k}): {len(v.__root__)}"
            else:
                message += f"\t{attribute}: {repr(instances)}"
    logger.info(message)


def main():
    parser = argparse.ArgumentParser(
        prog="python -m provision",
        description=(
            "Provisions complete sets of attributes from UBW to "
            "Omposteringsløsningen. Configured using a config file identical "
            "to the one used by ompostering-ms."
        ),
    )
    parser.add_argument(
        "-c",
        "--config",
        default=None,
        help=(
            "Path to config file to be used. Default: Same behavior as "
            "ompostering-ms, typically config.yaml if it exists."
        ),
    )
    parser.add_argument(
        "company_id",
        nargs="+",
        help=("company ID/firmakode of company to provision"),
    )
    parser.add_argument(
        "--no-confirm", action="store_true", help="do not ask for confirmation"
    )
    parser.add_argument(
        "--ignore-errors",
        action="store_true",
        help=(
            "ignore errors caused by individual attribute instances and carry "
            "out best effort provisioning"
        ),
    )

    attributes_group = parser.add_argument_group(
        "attributes",
        "Specify attributes to provision. Multiple can be used at the same time.",
    )
    attributes_group.add_argument(
        "-a",
        "--all",
        action="store_true",
    )
    for attribute in ATTRIBUTE_MODULES.keys():
        attributes_group.add_argument(
            f"--{attribute}",
            action="store_true",
        )
    parser.add_argument(
        "-w",
        "--max-workers",
        type=int,
        default=8,
        help="The maximum number of threads to use",
    )
    parser.add_argument(
        "-t",
        "--timeout",
        default=7200,
        type=int,
        help="The number of seconds to wait for the result",
    )
    args = parser.parse_args()

    # Set up context with configuration
    if args.config is not None and not os.path.isfile(args.config):
        logger.error("The specified config file does not exist: %s", args.config)
        sys.exit(ExitCode.NO_CONFIG_FILE)
    config_loader = get_config_loader(filename=args.config)
    setup_logging(config_loader)
    ctx = OmposteringMsContext(config_loader=config_loader)

    # Prompt for confirmation of URLs
    if not args.no_confirm:
        logger.info(
            "\nFetching data from base URL:\n\t%s\n\nPosting data to base URL:\n\t%s\n",
            ctx.ubw.urls.rest.base_url,
            ctx.ompostering.urls.baseurl,
        )
        if not _is_yes(input("Is it OK to use these URLs? [y/N]: ")):
            logger.error("Provisioning aborted by user.")
            sys.exit(ExitCode.USER_ABORT)

    # Determine which attributes to provision
    attributes: typing.List[str] = []
    for attribute in ATTRIBUTE_MODULES.keys():
        if getattr(args, attribute) or args.all:
            attributes.append(attribute)
    if not attributes:
        logger.error("No attributes specified for provisioning")
        sys.exit(ExitCode.NO_ATTRIBUTES)

    with concurrent.futures.ThreadPoolExecutor(
        max_workers=args.max_workers
    ) as executor:
        instances_ompostering = list(
            future.result(timeout=args.timeout)
            for future in concurrent.futures.as_completed(
                executor.submit(prepare_for_posting, ctx, args, attribute)
                for attribute in attributes
            )
        )

    # Prompt for confirmation for proceeding with posting
    if not args.no_confirm:
        if logger.isEnabledFor(logging.INFO):
            log_provision_info(instances_ompostering)
        if not _is_yes(input("\nDo you want to continue? [y/N]: ")):
            logger.info("Provisioning aborted by user.")
            sys.exit(ExitCode.USER_ABORT)

    for instances_list in instances_ompostering:
        for key in instances_list:
            _post_to_ompostering(
                ctx,
                instances_list,
                key,
                args.ignore_errors,
            )

    logger.info("Provisioning finished.")

    sys.exit(ExitCode.OK)


if __name__ == "__main__":
    main()
