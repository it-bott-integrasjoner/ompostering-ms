import logging
from typing import List, Sequence

import ompostering_client.models as om_models
import ubw_client.models as ubw_models

from ompostering_ms.context import OmposteringMsContext

logger = logging.getLogger(__name__)


def fetch_from_ubw(
    ctx: OmposteringMsContext,
    company_ids: List[str],
    ignore_errors: bool,
) -> Sequence[ubw_models.Periode]:
    perioder: Sequence[ubw_models.Periode] = []
    for i, company_id in enumerate(company_ids):
        try:
            logger.info(
                "Fetching perioder with company_id=%s (%s/%s)...",
                company_id,
                i + 1,
                len(company_ids),
            )
            perioder += ctx.ubw.get_perioder(company_id, period_type="GL")  # type: ignore[operator]
        except Exception as exc:
            logger.error(
                "Failed to fetch perioder with company_id=%s", company_id, exc_info=exc
            )
            if not ignore_errors:
                raise exc
    return perioder


def convert_ubw_to_ompostering(
    instances: Sequence[ubw_models.Periode],
    ignore_errors: bool,
) -> Sequence[om_models.Periode]:
    perioder_om: List[om_models.Periode] = []
    for periode_ubw in instances:
        try:
            perioder_om.append(om_models.Periode(**periode_ubw.dict()))
        except Exception as exc:
            logger.error(
                "Failed to convert periode with company_id=%s accounting_period=%s period_type=%s",
                periode_ubw.company_id,
                periode_ubw.accounting_period,
                periode_ubw.period_type.period_type,
                exc_info=exc,
            )
            if not ignore_errors:
                raise exc
    return perioder_om


def post_to_ompostering(
    ctx: OmposteringMsContext,
    instances: Sequence[om_models.Periode],
    ignore_errors: bool,
):
    for i, periode in enumerate(instances):
        # TODO: This loop is a potential candidate for parallelization
        logger.info(
            "Posting periode with company_id=%s accounting_period=%s (%s/%s)...",
            periode.company_id,
            periode.accounting_period,
            i + 1,
            len(instances),
        )
        try:
            ctx.ompostering.post_periode(periode)
        except Exception as exc:
            logger.error(
                "Failed to post periode with company_id=%s accounting_period=%s",
                periode.company_id,
                periode.accounting_period,
                exc_info=exc,
            )
            if not ignore_errors:
                raise exc
