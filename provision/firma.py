import logging
from typing import List, Sequence

import ompostering_client.models as om_models
import ubw_client.models as ubw_models

from ompostering_ms.context import OmposteringMsContext

logger = logging.getLogger(__name__)


def fetch_from_ubw(
    ctx: OmposteringMsContext,
    company_ids: List[str],
    ignore_errors: bool,
) -> Sequence[ubw_models.Firma]:
    firmaer: List[ubw_models.Firma] = []
    for i, company_id in enumerate(company_ids):
        try:
            logger.info(
                "Fetching firma with ID=%s (%s/%s)...",
                company_id,
                i + 1,
                len(company_ids),
            )
            firmaer.append(ctx.ubw.get_firma(company_id))  # type: ignore[arg-type]
        except Exception as exc:
            logger.error("Failed to fetch firma with ID=%s", company_id, exc_info=exc)
            if not ignore_errors:
                raise exc
    return firmaer


def convert_ubw_to_ompostering(
    instances: Sequence[ubw_models.Firma],
    ignore_errors: bool,
) -> Sequence[om_models.BaseModel]:
    firmaer_om: List[om_models.Firma] = []
    for firma_ubw in instances:
        try:
            firmaer_om.append(om_models.Firma(**firma_ubw.dict()))
        except Exception as exc:
            logger.error(
                "Failed to convert firma with ID=%s", firma_ubw.company_id, exc_info=exc
            )
            if not ignore_errors:
                raise exc
    return firmaer_om


def post_to_ompostering(
    ctx: OmposteringMsContext,
    instances: Sequence[om_models.Firma],
    ignore_errors: bool,
):
    for i, firma in enumerate(instances):
        logger.info(
            "Posting firma with ID=%s (%s/%s)...",
            firma.company_id,
            i + 1,
            len(instances),
        )
        try:
            ctx.ompostering.post_firma(firma)
        except Exception as exc:
            logger.error(
                "Failed to post firma with ID=%s", firma.company_id, exc_info=exc
            )
            if not ignore_errors:
                raise exc
