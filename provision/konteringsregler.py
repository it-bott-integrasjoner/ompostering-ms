import collections
import functools
import logging
from typing import DefaultDict, List, Sequence, Tuple

import ompostering_client.models as om_models
import ubw_client.models as ubw_models

from ompostering_ms.context import OmposteringMsContext

logger = logging.getLogger(__name__)


def fetch_from_ubw(
    ctx: OmposteringMsContext,
    company_ids: List[str],
    ignore_errors: bool,
) -> Sequence[Tuple[str, ubw_models.Konteringsregel]]:
    konteringsregler: List[Tuple[str, ubw_models.Konteringsregel]] = []
    for i, company_id in enumerate(company_ids):
        try:
            logger.info(
                "Fetching konteringsregler with company_id=%s (%s/%s)...",
                company_id,
                i + 1,
                len(company_ids),
            )
            konteringsregler.append(
                (company_id, ctx.ubw.get_konteringsregler(company_id))  # type: ignore[arg-type]
            )
        except Exception as exc:
            logger.error(
                "Failed to fetch konteringsregler with company_id=%s",
                company_id,
                exc_info=exc,
            )
            if not ignore_errors:
                raise exc
    return konteringsregler


def convert_ubw_to_ompostering(
    instances: Sequence[Tuple[int, List[ubw_models.Konteringsregel]]],
    ignore_errors: bool,
) -> DefaultDict[str, om_models.Konteringsregler]:
    konteringsregler_om: collections.defaultdict = collections.defaultdict(
        functools.partial(om_models.Konteringsregler, __root__=[])
    )

    for company_id, konteringsregler_ubw in instances:
        for konteringsregel_ubw in konteringsregler_ubw:
            try:
                konteringsregler_om[company_id].__root__.append(
                    om_models.Konteringsregel(**konteringsregel_ubw.dict())
                )
            except Exception as exc:
                logger.error(
                    "Failed to convert konteringsregel with ID=%s",
                    konteringsregel_ubw.account_rule,
                    exc_info=exc,
                )
                if not ignore_errors:
                    raise exc
    return konteringsregler_om


def post_to_ompostering(
    ctx: OmposteringMsContext,
    konteringsregler: DefaultDict[str, om_models.Konteringsregler],
    ignore_errors: bool,
):
    logger.info("Posting konteringsregler")
    try:
        for company_id, regler in konteringsregler.items():
            ctx.ompostering.post_konteringsregler(company_id, regler)
    except Exception as exc:
        logger.error("Failed to post konteringsregler", exc_info=exc)
        if not ignore_errors:
            raise exc
