import logging
import typing

import ompostering_client.models as om_models
import ubw_client.models as ubw_models

from ompostering_ms.context import OmposteringMsContext

logger = logging.getLogger(__name__)


def fetch_from_ubw(
    ctx: OmposteringMsContext,
    company_ids: typing.List[str],
    ignore_errors: bool,
) -> typing.Sequence[ubw_models.Kontoplan]:
    kontoplaner = []
    for i, company_id in enumerate(company_ids):
        try:
            logger.info(
                "Fetching kontoplaner with company_id=%s (%s/%s)...",
                company_id,
                i + 1,
                len(company_ids),
            )
            kontoplaner.append(ctx.ubw.get_kontoplan(company_id))
        except Exception as exc:
            logger.error(
                "Failed to fetch kontoplaner with company_id=%s",
                company_id,
                exc_info=exc,
            )
            if not ignore_errors:
                raise exc
    return kontoplaner


def convert_ubw_to_ompostering(
    instances: typing.Sequence[ubw_models.Kontoplan],
    ignore_errors: bool,
) -> typing.Sequence[om_models.Kontoplan]:
    kontoplans: typing.List[om_models.Kontoplan] = []
    for plan in instances:
        try:
            kontoplans.append(om_models.Kontoplan(**plan.dict()))
        except Exception as exc:
            logger.error(
                "Failed to convert kontoplan with company_id=%s",
                plan.company_id,
                exc_info=exc,
            )
            if not ignore_errors:
                raise exc
    return kontoplans


def post_to_ompostering(
    ctx: OmposteringMsContext,
    instances: typing.Sequence[om_models.Kontoplan],
    ignore_errors: bool,
):
    for i, kontoplan in enumerate(instances):
        # TODO: This loop is a potential candidate for parallelization
        logger.info(
            "Posting kontoplan with company_id=%s (%s/%s)...",
            kontoplan.company_id,
            i + 1,
            len(instances),
        )
        try:
            ctx.ompostering.post_kontoplaner(kontoplan.company_id, kontoplan.accounts)
        except Exception as exc:
            logger.error(
                "Failed to post kontoplan with company_id=%s",
                kontoplan.company_id,
                exc_info=exc,
            )
            if not ignore_errors:
                raise exc
