import time
import sys
import logging
import json
import types
import signal
import sqlite3
from typing import Any, List, Optional, Tuple

import cx_Oracle  # type: ignore
import pika  # type: ignore

from ompostering_ms.config import get_config, OmposteringMsConfig
from pika_context_manager import PCM

config = get_config()
logger = logging.getLogger(__name__)
level = logging.getLevelName(config.cqn.logging_level)
logger.setLevel(level)
console = logging.StreamHandler(sys.stdout)
logger.addHandler(console)


def send_message(
    config: OmposteringMsConfig, pcm: PCM, batch_id: str, firm: str
) -> bool:
    message = json.dumps({"batchid": batch_id, "firma": firm})
    if pcm.publish(
        config.notification_publisher.exchange,
        routing_key=config.notification_publisher.routing_key,
        message=message,
        content_type="application/json",
    ):
        logger.info(
            "Published %s with routing key %s to exchange %s",
            message,
            config.notification_publisher.routing_key,
            config.notification_publisher.exchange,
        )
        return True
    else:
        logger.error(
            "Failed to publish %s with routing key %s to exchange %s",
            message,
            config.notification_publisher.routing_key,
            config.notification_publisher.exchange,
        )
        return False


def connect_oracle(config: OmposteringMsConfig) -> Optional[pika.connection.Connection]:
    dsn = cx_Oracle.makedsn(config.cqn.server, config.cqn.port, config.cqn.sid)
    return cx_Oracle.connect(
        user=config.cqn.user, password=config.cqn.password, dsn=dsn, events=True
    )


def select_events(
    config: OmposteringMsConfig, connection: pika.connection.Connection
) -> Any:
    cursor = connection.cursor()
    cursor.execute(
        "SELECT batch_id, firma FROM "
        "ompostering.omp_hode_hist WHERE "
        "resultat is null AND firma = :firm",
        firm=config.cqn.firm,
    )
    return cursor.fetchall()


def connect_storage() -> Tuple[pika.connection.Connection, Any]:
    con = sqlite3.connect(config.cqn.sent_events_storage)
    con.execute("""CREATE TABLE IF NOT EXISTS sent (batch_id int, firm text)""")
    con.commit()
    return con, con.cursor()


def is_sent(cursor: Any, batch_id: str, firm: str) -> bool:
    cursor.execute(
        "SELECT 1 FROM sent WHERE batch_id = ? AND firm = ?", (batch_id, firm)
    )
    if cursor.fetchone():
        return True
    else:
        return False


def store_sent_state(
    connection: pika.connection.Connection,
    cursor: Any,
    batch_id: str,
    firm: str,
) -> None:
    cursor.execute("INSERT INTO sent VALUES (?, ?)", (batch_id, firm))
    connection.commit()


def publish() -> None:
    state = types.SimpleNamespace(run=True)

    storage, cursor = connect_storage()

    def stop(_signum: Any, _frame: Any) -> None:
        state.run = False

    signal.signal(signal.SIGINT, stop)
    signal.signal(signal.SIGTERM, stop)

    oracle = connect_oracle(config)

    while state.run:
        events = (
            (batch_id, firm)
            for (batch_id, firm) in select_events(config, oracle)
            if not is_sent(cursor, batch_id, firm)
        )
        if events:
            with PCM(**dict(config.notification_publisher.mq.connection)) as pcm:
                for batch_id, firm in events:
                    if send_message(config, pcm, batch_id, firm):
                        store_sent_state(storage, cursor, batch_id, firm)

        time.sleep(config.cqn.sleeping_time)

    storage.close()


if __name__ == "__main__":
    publish()
