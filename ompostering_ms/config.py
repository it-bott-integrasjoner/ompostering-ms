import functools
import os
import sys
from http import HTTPStatus
from typing import Dict, Iterator, List, Optional, Union, Any, FrozenSet, TYPE_CHECKING

import yaml
from pydantic import (
    BaseModel,
    Extra,
    PositiveInt,
    ConstrainedInt,
    NonNegativeFloat,
    Field,
    root_validator,
)
from ubw_client.models import UbwClientConfig
from ompostering_client.models import OmposteringClientConfig
from urllib3 import Retry

SERVICE_NAME = __name__.split(".")[0]

FILES_ETC = os.path.join("" if sys.prefix == "/usr" else sys.prefix, "etc")

CONFIG_NAME = "config.yaml"
CONFIG_LOOKUP_ORDER = [
    os.getcwd(),
    os.path.expanduser(f"~/{SERVICE_NAME}"),
    os.path.join(FILES_ETC, f"{SERVICE_NAME}"),
]
CONFIG_ENVIRON = f"{SERVICE_NAME.upper()}_CONFIG"


class SetraClientConfig(BaseModel):
    url: str
    headers: Optional[Union[None, Dict[str, Any]]]
    return_objects: Optional[bool]
    use_sessions: Optional[bool]


class CqnClientConfig(BaseModel):
    server: str
    port: int
    sid: str
    user: str
    password: str
    callback_port: int
    timeout: int
    logging_level: str
    logging_to_console: str
    sleeping_time: int
    pick_up_old_batches_time_delay: int
    sent_events_storage: str = "ompostering-publisher.db"
    firm: str


class Connection(BaseModel):
    hostname: str
    port: int
    virtual_host: str
    username: str
    password: str
    tls_on: bool
    server_name_indication: bool


class Mq(BaseModel):
    connection: Connection


class NotificationPublisherClientConfig(BaseModel):
    mq: Mq
    exchange: str
    routing_key: str


if TYPE_CHECKING:
    HttpErrorStatus = int
else:

    class HttpErrorStatus(ConstrainedInt):
        ge = 400
        lt = 600


# Properties of this class are used to construct a urllib3.Retry object
# See https://urllib3.readthedocs.io/en/stable/reference/urllib3.util.html#urllib3.util.Retry
#
# To print the sleep times in seconds (try different values of total and backoff_factor):
#     total=5; backoff_factor = 10; print([backoff_factor * (2 ** (x - 1)) for x in range(1, total + 1)])
class RetryConfig(BaseModel):
    total: PositiveInt = 5
    status_forcelist: FrozenSet[HttpErrorStatus] = frozenset(
        {
            HTTPStatus.UNAUTHORIZED,
            HTTPStatus.INTERNAL_SERVER_ERROR,
            HTTPStatus.BAD_GATEWAY,
            HTTPStatus.SERVICE_UNAVAILABLE,
            HTTPStatus.GATEWAY_TIMEOUT,
        }
    )
    allowed_methods: FrozenSet[str] = Field(
        frozenset({"POST", "GET", "PUT", "PATCH", "DELETE"}),
        min_items=1,
    )
    backoff_factor: NonNegativeFloat = 2

    @root_validator
    def check_model(cls, values: Dict[str, Any]) -> Dict[str, Any]:
        Retry(**values)
        return values

    def get_urllib3_retry(self) -> Retry:
        return Retry(**self.dict())

    class Config:
        extra = Extra.forbid
        allow_mutation = False


class OmposteringMsConfig(BaseModel):
    """ompostering-ms configuration"""

    ubw: UbwClientConfig
    ompostering: OmposteringClientConfig
    cqn: CqnClientConfig
    notification_publisher: NotificationPublisherClientConfig
    setra: SetraClientConfig
    lock_dsn: str
    http_retry: RetryConfig = RetryConfig()


class OmposteringMsConfigLoader(BaseModel):
    """ompostering-ms configuration loader"""

    ompostering_ms: OmposteringMsConfig
    sentry: Optional[Dict[str, Any]]
    logging: Optional[Dict[str, Any]]
    filename: Optional[str]

    @classmethod
    def from_yaml(cls, yamlstr: str) -> "OmposteringMsConfigLoader":
        return cls(**yaml.load(yamlstr, Loader=yaml.FullLoader))

    @classmethod
    def from_file(cls, filename: str) -> "OmposteringMsConfigLoader":
        with open(filename, "r") as f:
            res = cls.from_yaml(f.read())
            res.filename = filename
            return res


def iter_config_locations(
    filename: str = CONFIG_NAME, lookup_order: Optional[List[str]] = None
) -> Iterator[str]:
    if CONFIG_ENVIRON in os.environ:
        yield os.path.abspath(os.environ[CONFIG_ENVIRON])
    for path in lookup_order or CONFIG_LOOKUP_ORDER:
        yield os.path.abspath(os.path.join(path, filename))


@functools.lru_cache()
def get_config_loader(filename: str = None) -> OmposteringMsConfigLoader:
    if filename is None:
        for filename in filter(os.path.exists, iter_config_locations()):
            break
    if filename:
        return OmposteringMsConfigLoader.from_file(filename)
    raise ValueError("Configuration not found")


@functools.lru_cache()
def get_config(filename: str = None) -> OmposteringMsConfig:
    return get_config_loader(filename).ompostering_ms


@functools.lru_cache()
def get_logging_config(filename: str = None) -> Union[Dict[str, Any], None, Any]:
    if filename is None:
        for filename in filter(os.path.exists, iter_config_locations()):
            break
    if filename:
        with open(filename) as f:
            logging_config = yaml.load(f.read(), Loader=yaml.FullLoader).get(
                "logging", None
            )
            return logging_config
    return None
