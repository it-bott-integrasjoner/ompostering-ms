import json
import collections.abc
from typing import Any, Dict, Tuple

from tofh.plugins.mq.event import Event


def event_transform(event: Event) -> Tuple[Tuple[Any, ...], Dict[str, Any]]:
    """
    Transform a tofh event to a tuple and a dict.

    Expects the body of the event to be json. If an args key is present the content is
    put in the args tuple, while the remaining keys are returned in the body_dict.
    """
    body_dict = json.loads(event.body)
    args = body_dict.pop("args", None)
    if args is None:
        return tuple(), body_dict
    elif isinstance(args, collections.abc.Sequence):
        return tuple(args), body_dict
    return (args,), body_dict


def setra_transform(event: Event) -> Tuple[Tuple[Any, ...], Dict[str, Any]]:
    """
    Transform a tofh event from setra to args, dict format

    If the body contains a "source" key we know that the event has a body with the new
    message format from setra. This means that there is no "identifier" key, and we
    create it from the last element of the source field.
    """
    args, body_dict = event_transform(event)
    if "source" in body_dict:
        body_dict["identifier"] = body_dict["source"].split(":")[-1]
    return args, body_dict
