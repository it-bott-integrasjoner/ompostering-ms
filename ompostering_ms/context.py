import logging
from urllib3 import Retry
from functools import cached_property


from ompostering_client import OmposteringClient
from ompostering_client.client import get_client
from requests import Session
from requests.adapters import HTTPAdapter
from setra_client import SetraClient
from setra_client.client import get_client as get_setra_client
from ubw_client.client import UBWRestClient, get_rest_client as get_ubw_rest_client
from urllib3 import Retry

from ompostering_ms.config import OmposteringMsConfig, OmposteringMsConfigLoader


logger = logging.getLogger(__name__)


class OmposteringMsContext:
    """
    Container for singleton objects.

    The general idea is to pass around this object, so that its attributes acts
    as singletons.
    """

    def __init__(self, config_loader: OmposteringMsConfigLoader):
        self.config_loader = config_loader

    @cached_property
    def config(self) -> OmposteringMsConfig:
        return self.config_loader.ompostering_ms

    @cached_property
    def ubw(self) -> UBWRestClient:
        """
        Client for accessing ubw
        """
        client = get_ubw_rest_client(rest=self.config.ubw.rest)
        client.rest_session = self.requests_session
        return client

    @cached_property
    def ompostering(self) -> OmposteringClient:
        """
        Client for writing directly to the ompostering database
        """
        client = get_client(self.config.ompostering.dict())
        client.session = self.requests_session
        return client

    @cached_property
    def setra(self) -> SetraClient:
        """
        Client for using SETRA's API
        """
        client = get_setra_client(self.config.setra.dict())
        client.session = self.requests_session
        return client

    @cached_property
    def urllib3_retry(self) -> Retry:
        return self.config.http_retry.get_urllib3_retry()

    @property
    def requests_session(self) -> Session:
        session = Session()
        session.mount("https://", HTTPAdapter(max_retries=self.urllib3_retry))
        session.mount("http://", HTTPAdapter(max_retries=self.urllib3_retry))
        return session
