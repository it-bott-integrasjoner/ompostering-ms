import datetime
import functools
import logging
from typing import Any, Dict, Optional, Union
from types import SimpleNamespace

import celery.app.task
from ompostering_client.models import (
    Arbeidsordre,
    Avgiftskode,
    Begrepsverdi,
    Bilagstype,
    Bruker,
    Firma,
    Konteringsregel,
    Kontoplan,
    OmpHodeError,
    Periode,
    Prosjekt,
    SetraBatch,
    TransaksjonHodeError,
)
import ubw_client.models as ubw_models
from requests import HTTPError
from lock_context_manager import locker
from setra_client.models import (
    InputBatch,
    CompleteBatch,
    Transaction,
    Voucher,
    ResponseStatusEnum,
)

from ompostering_ms.config import get_config, get_config_loader
from ompostering_ms.context import OmposteringMsContext

logger = logging.getLogger(__name__)

JsonType = Any

# Only provision attribute values with these attribute IDs
# Also used in `provision/bergrepsverdier.py`
PROVISIONED_ATTRIBUTE_IDS = (
    "A0",  # Konto (KONTO)
    "C1",  # Koststed (KOSTSTED)
    "B0",  # Prosjekt (PROSJEKT)
    "BF",  # Delprosjekt (ARBORDRE)
    "F0",  # Anlegg (ANLEGG)
    "C0",  # Ansattnummer (ANSATTNR)
    # TODO: "??",  # =Byggnr (_____)
    "B1",  # Arbeidspakke (AKTIVITET)
    "AX",  # Bilagsart (BIL.ART)
    "A1",  # Avgiftskode (AV)
    "AW",  # Regnskapsperiode (PERIODE)
)


def arbeidsordre_ubw_to_om(data: ubw_models.Arbeidsordre) -> Arbeidsordre:
    # NOTE: This function is also used in `provision/arbeidsordrer.py`
    return Arbeidsordre(
        company_id=data.company_id,
        work_order_id=data.work_order_id,
        delprosjekt_fra_dato=data.date_from,
        delprosjekt_til_dato=data.date_to,
        regnskapsperiode_fra=data.period_from,
        regnskapsperiode_til=data.period_to,
        delprosjekt_statuskode=data.status,
        delprosjekt_navn=data.work_order_name,
        delprosjekt_prosjektnr=data.project_id,
        delprosjekt_koststed=data.cost_centre,
        delprosjekt_leder_ansattnr=data.project_manager_id,
        delprosjekt_finansieringskilde=data.category1,
        last_updated_at=str(data.last_updated.updated_at),
        last_updated_by=str(data.last_updated.updated_by),
    )


def _complete_batch_to_ompostering_batch(
    batch_id: str, batch: CompleteBatch, error_data: Dict[str, Any]
) -> SetraBatch:
    """Convert output from SETRA to Ompostering format.

    Converts all values to str (even None to "None") except for voucherno_ubw and
    voucherno_ubw_wflow. We do this because it was done when this lived in SETRA, and
    we don't want to change the format when we don't even know the database schema
    we're saving this to.
    """

    def obj_to_dict(obj: object) -> Dict[str, Any]:
        if not isinstance(obj, dict):
            obj = obj.__dict__
        for key in obj.keys():
            obj[key] = str(obj[key])
        return obj

    voucherno_ubw = batch.vouchers[0].voucherno_ubw
    voucherno_ubw_wflow = batch.vouchers[0].voucherno_ubw_wflow
    data = obj_to_dict(batch)

    # We select the first voucher, since ompostering does not relate to
    # vouchers.
    data["voucherno_ubw"] = voucherno_ubw
    data["voucherno_ubw_wflow"] = voucherno_ubw_wflow
    data["id"] = batch_id
    data["batchid_interface"] = data["batchid"]

    data["batch_errors"] = error_data["batch_errors"]

    voucher_errors = error_data["voucher_errors"]
    for ve in voucher_errors:
        ve["voucher"] = ve["voucher_id"]
        del ve["voucher_id"]
    data["voucher_errors"] = voucher_errors

    transaction_errors = error_data["transaction_errors"]
    for te in transaction_errors:
        te["transaction"] = te["transaction_id"]
        del te["transaction_id"]
    data["transaction_errors"] = error_data["transaction_errors"]

    setra_batch = SetraBatch(**data)

    return setra_batch


def _is_double_submission_to_setra(
    status: ResponseStatusEnum,
    resp: Dict[str, Union[int, JsonType, bytes, None]],
) -> bool:
    """
    Current answer format in Setra in case of double submission is
    {'code': 409, 'content': {'__all__': [['Bunt with this Firma, Avs system and Buntnr already exists.']]}}
    This method checks for the match to this pattern and returns False unless
    a clear match is found.
    """
    if status.value != ResponseStatusEnum.CONFLICT:
        return False
    else:
        try:
            pattern_found = "already exists" in str(resp["content"]["__all__"])
            return pattern_found
        except Exception:
            return False


def ensure_context(o: Union[SimpleNamespace, celery.app.task.Task]) -> None:
    """Ensure a task has access to config and clients."""
    if not hasattr(o, "context"):
        o.context = OmposteringMsContext(config_loader=get_config_loader())


def health_check(self: Union[SimpleNamespace, celery.app.task.Task]) -> None:
    # ensure that context data can be loaded
    try:
        ensure_context(self)
        logger.debug("Ompostering-ms passed health check")
    except:
        logger.error("Ompostering-ms failed health check")


stateless_health_check = functools.partial(health_check, SimpleNamespace())


def ensure_project(
    self: Union[SimpleNamespace, celery.app.task.Task], *args: str, **kwargs: Any
) -> Optional[bool]:
    """
    Keep project info updated by reacting to messages from UBW.

    The handler expects the following payload:
    {
        "Client":"9900",
        "Id":"10000126",
    }
    which should be sent through the event_transform transformation so that the fields
    are accessible as kwargs, e.g kwargs['Client'].

    Meant to react to messages from UBW with routing key "prosjekter".
    """
    # Ensure we have access to the necessary clients
    ensure_context(self)
    ctx = self.context

    # Ignore if delete operation
    # Replace with logic for removal if ompostering api supports it in
    # the future
    if kwargs.get("Operation") == "delete":
        logger.info("Nothing to do for Operation delete, skipping")
        return None
    try:
        project_id = kwargs["Id"]
    except KeyError:
        raise ValueError("Id is missing or null")
    try:
        company_id = kwargs["Client"]
    except KeyError:
        raise ValueError("Client is missing or null")

    project = ctx.ubw.get_prosjekt(company_id, project_id)
    project = Prosjekt(**project.dict())
    ctx.ompostering.post_prosjekt(project)
    return True


stateless_ensure_project = functools.partial(ensure_project, SimpleNamespace())


def ensure_konteringsregel(
    self: Union[SimpleNamespace, celery.app.task.Task], *args: str, **kwargs: Any
) -> Optional[bool]:
    """
    Keep konteringsregler updated by reacting to messages from UBW.

    The handler expects the following payload:
    {
        "Client":"9900",
        "Id":"10000126",
    }
    which should be sent through the event_transform transformation so that the fields
    are accessible as kwargs, e.g kwargs['Client'].

    Meant to react to messages from UBW with routing key "konteringsregler".
    """
    # Ensure we have access to the necessary clients
    ensure_context(self)
    ctx = self.context

    try:
        konteringsregel_id = kwargs["Id"]
    except KeyError:
        raise ValueError("Id is missing or null")
    try:
        company_id = kwargs["Client"]
    except KeyError:
        raise ValueError("Client is missing or null")

    ubw_konteringsregel = ctx.ubw.get_konteringsregel(company_id, konteringsregel_id)
    om_konteringsregel = Konteringsregel(**ubw_konteringsregel.dict())
    ctx.ompostering.post_konteringsregel(om_konteringsregel)
    return True


stateless_ensure_konteringsregel = functools.partial(
    ensure_konteringsregel, SimpleNamespace()
)


def ensure_company(
    self: Union[SimpleNamespace, celery.app.task.Task], *args: str, **kwargs: Any
) -> Optional[bool]:
    """
    Keep company info updated by reacting to messages from UBW.

    The handler expects the following payload:
    {
        "Client":"9900",
    }
    which should be sent through the event_transform transformation so that the fields
    are accessible as kwargs, e.g kwargs['Client'].

    Meant to react to messages from UBW with routing key "firma".
    """
    # Ensure we have access to the necessary clients
    ensure_context(self)
    ctx = self.context

    try:
        company_id = kwargs["Client"]
    except KeyError:
        raise ValueError("Client is missing or null")

    company = ctx.ubw.get_firma(company_id)
    company = Firma(**company.dict())
    ctx.ompostering.post_firma(company)
    return True


stateless_ensure_company = functools.partial(ensure_company, SimpleNamespace())


def ensure_attribute_values(
    self: Union[SimpleNamespace, celery.app.task.Task], *args: str, **kwargs: Any
) -> Optional[bool]:
    """
    Keep begrepsverdier updated by reacting to messages from UBW.

    The handler expects the following payload:
    {
        "AttributeId": "B1",
        "PeriodFrom": "0",
        "Client": "UB",
        "Id": "100104",
        "LastUpdate": "2021-01-04 12.27.50",
        "Url": "api.dfo.no/aktiviteter/v1/UB/{id}",
        "Operation": "update",
        "OperationOrder": "2"
    }

    which should be sent through the event_transform transformation so that the fields
    are accessible as kwargs, e.g kwargs['Client'].

    Meant to react to messages from UBW with routing key "anlegg".
    """
    # Ensure we have access to the necessary clients
    ensure_context(self)
    ctx = self.context

    try:
        company_id = kwargs["Client"]
    except KeyError:
        raise ValueError("Client is missing or null")
    try:
        attribute_id = kwargs["AttributeId"]
    except KeyError:
        raise ValueError("AttributeId is missing or null")
    try:
        attribute_value = kwargs["Id"]
    except KeyError:
        raise ValueError("Id is missing or null")

    if attribute_id not in PROVISIONED_ATTRIBUTE_IDS:
        return True

    values = ctx.ubw.get_begrepsverdier(
        company_id, attribute_id, begrep_verdi=attribute_value
    )
    values = [Begrepsverdi(**x.dict()) for x in values]
    errors = []
    for value in values:
        try:
            ctx.ompostering.post_begrepsverdi(value)
        except HTTPError:
            json = value.json(by_alias=True)
            errors.append(json)
    if len(errors) > 0:
        raise ValueError("Error posting %s values: %s" % (len(errors), errors))
    return True


stateless_ensure_attribute_values = functools.partial(
    ensure_attribute_values, SimpleNamespace()
)


def ensure_anlegg(
    self: Union[SimpleNamespace, celery.app.task.Task], *args: str, **kwargs: Any
) -> None:
    """
    React to messages from UBW about changes to anlegg.


    The handler expects a payload with at least these fields:
    {
        "Client":"72",
        "Id":"10000011",
    }
    which should be sent through the event_transform transformation so that the fields
    are accessible as kwargs, e.g kwargs['Client'].

    Meant to react to messages from UBW with routing key "anlegg".
    """
    # Ensure we have clients
    ensure_context(self)
    ctx = self.context
    # Ensure payload matches expectations
    try:
        firma_id = kwargs["Client"]
    except KeyError:
        raise ValueError("Client is missing")
    try:
        anlegg_id = kwargs["Id"]
    except KeyError:
        raise ValueError("Id is missing")

    # Fetch from ubw, convert and post new values to ompostering
    errors = []
    ubw_values = ctx.ubw.get_anlegg(firma_id, anlegg_id)
    omp_ready = [Begrepsverdi(**x.dict()) for x in ubw_values]
    for value in omp_ready:
        try:
            ctx.ompostering.post_begrepsverdi(value)
        except HTTPError:
            json = value.json(by_alias=True)
            errors.append(json)
    if len(errors) > 0:
        raise ValueError("Error posting %s values: %s" % (len(errors), errors))


stateless_ensure_anlegg = functools.partial(ensure_anlegg, SimpleNamespace())


def ensure_koststed(
    self: Union[SimpleNamespace, celery.app.task.Task], *args: str, **kwargs: Any
) -> None:
    """
    React to messages from UBW about changes to koststeder.

    The handler expects a payload with at least these fields:
    {
        "Client":"72",
        "Id":"10000011",
    }
    which should be sent through the event_transform transformation so that the fields
    are accessible as kwargs, e.g kwargs['Client'].

    Meant to react to messages from UBW with routing key "koststeder".
    """
    # Ensure we have access to the necessary clients
    ensure_context(self)
    ctx = self.context
    # Ensure payload matches expectations
    try:
        company_id = kwargs["Client"]
    except KeyError:
        raise ValueError("Client is missing")
    try:
        koststed_id = kwargs["Id"]
    except KeyError:
        raise ValueError("Id is missing")
    # Fetch from ubw, convert and post new values to ompostering
    errors = []
    ubw_values = ctx.ubw.get_koststed(company_id, koststed_id)
    omp_ready = [Begrepsverdi(**x.dict()) for x in ubw_values]
    for value in omp_ready:
        try:
            ctx.ompostering.post_begrepsverdi(value)
        except HTTPError:
            json = value.json(by_alias=True)
            errors.append(json)
    if len(errors) > 0:
        raise ValueError("Error posting %s values: %s" % (len(errors), errors))


stateless_ensure_koststed = functools.partial(ensure_koststed, SimpleNamespace())


def ensure_transaksjon_to_setra(
    self: Union[SimpleNamespace, celery.app.task.Task], *args: str, **kwargs: Any
) -> None:
    """Send transactions to SETRA.

    Meant to react to messages from ompostering-publisher with routing key "omp"

    Done by fetching the ompostering head, transaction head, and transaction lines from
    ompostering, combining the info, and sending to setra with setra_client.

    TODO: Move hardcoded magic values "GL" and "OP" to config.
    """
    # Ensure we have access to the necessary clients
    ensure_context(self)
    ctx = self.context

    try:
        batch_id = kwargs["batchid"]
    except KeyError:
        raise ValueError("BatchID is missing")
    try:
        firma = kwargs["firma"]
    except KeyError:
        raise ValueError("Firma is missing")

    transaksjons_hoder = ctx.ompostering.get_transaksjon_hode(batch_id, firma)
    if not transaksjons_hoder:
        logger.error(
            "No transaksjons_hoder found for batchID %s and firma %s, ignoring",
            batch_id,
            firma,
        )
        return None
    if isinstance(transaksjons_hoder, TransaksjonHodeError):
        raise ValueError(
            "Error in extracting transaction for batchid: %s, "
            "firma: %s, error_code: %s"
            % (batch_id, firma, transaksjons_hoder.error_code),
        )
    if len(transaksjons_hoder) != 1:  # TODO: What if len(transaksjons_hoder) != 1?
        raise AssertionError("Should have exactly 1 TransaksjonHode by now")
    transaksjons_hode = transaksjons_hoder[0]

    transaksjons_linje = ctx.ompostering.get_transaksjon_linje(batch_id, firma)
    if transaksjons_linje is None:  # 404 or any other non-error response except 200
        raise RuntimeError("TODO: Return or raise??")  # TODO

    omp_hode = ctx.ompostering.get_omp_hode(firma, batch_id)

    if isinstance(omp_hode, OmpHodeError):
        raise ValueError(
            f"Error in extracting OmpHode for batchid: {batch_id}, firma: {firma},"
            f" error_code: {omp_hode.error_code}"
        )
    bruker = omp_hode[0].bruker  # TODO: What if len(omp_hode) != 1
    # The simplest email check in history
    if "@" not in bruker:
        raise ValueError(
            f"Bruker field from omp_hode api is not an email. Can't create ext_inv_ref"
            f" field for batchid: {batch_id}, firma: {firma}"
        )
    transaksjoner = []
    for trans in transaksjons_linje:
        if not trans.beskrivelse:
            raise ValueError(
                f"Description field in a transaction for batchid: {batch_id}, firma: {firma} "
                f"is empty which is forbidden. Cannot send batch further."
            )

        if trans.dato:
            trans_date = datetime.datetime.strptime(trans.dato, "%d.%m.%Y")
        else:
            trans_date = datetime.datetime.combine(
                datetime.date.today(), datetime.datetime.min.time()
            )

        extinvref = f"AvsID=OP;BuntNr=OP{batch_id:0>7};Epost={bruker}"[:100]
        transaksjoner.append(
            Transaction(
                account=trans.art,
                amount=trans.belop,
                transdate=trans_date,
                transtype="GL",
                curamount=None,
                currency=None,
                description=trans.beskrivelse,
                dim1=trans.sted,
                dim2=None,
                dim3=None,
                dim4=None,
                dim5=trans.delprosjekt,
                dim6=trans.ansattnr,
                dim7=trans.aktivitet,
                taxcode=trans.mva,
                sequenceno=trans.sequenceno,
                extinvref=extinvref,
            )
        )

    v = Voucher(
        voucherdate=transaksjons_hode.voucherdate,
        exref=transaksjons_hode.exref,
        voucherno=1,
        transactions=transaksjoner,
    )

    batch = InputBatch(
        client=firma,
        batchid=batch_id,
        period=transaksjons_hode.periode,
        interface="OP",
        vouchertype=transaksjons_hode.vouchertype,
        vouchers=[v],
    )

    status, resp = ctx.setra.post_new_batch(batch)
    if status != ResponseStatusEnum.ACCEPTED:
        if _is_double_submission_to_setra(status, resp):
            logger.error(
                "Double submission to Setra attempted for batchID %s and firma %s, not retrying",
                batch_id,
                firma,
            )
            return None
        else:
            raise ValueError(f"Unexpected status: {status.value}. Response: {resp}")


stateless_ensure_transaksjon_to_setra = functools.partial(
    ensure_transaksjon_to_setra, SimpleNamespace()
)


def ensure_period(
    self: Union[SimpleNamespace, celery.app.task.Task], *args: str, **kwargs: Any
) -> Optional[bool]:
    """
    Keep period info updated.

    The handler expects the following payload:
    {
        "Client":"9900",
        "Id":"10000126",
    }
    where the fields are accessibles as kwargs, e.g kwargs['Id']

    Meant to react to messages from UBW with routing key "regnskapsperioder".
    """

    # Ensure we have access to the necessary clients
    ensure_context(self)
    ctx = self.context

    try:
        company_id = kwargs["Client"]
    except KeyError:
        raise ValueError("Client is missing or null")
    try:
        accounting_period = kwargs["Id"]
    except KeyError:
        raise ValueError("Id is missing or null")

    period = ctx.ubw.get_periode(company_id, accounting_period)
    period = Periode(**period.dict())
    ctx.ompostering.post_periode(period)
    return True


stateless_ensure_period = functools.partial(ensure_period, SimpleNamespace())


def ensure_arbeidsordre(
    self: Union[SimpleNamespace, celery.app.task.Task], *args: str, **kwargs: Any
) -> Optional[bool]:
    """
    Keep arbeidsordre-info updated.

    The handler expects a payload with at least these fields:
    {
        "Client": "72",
        "Id":"10000126",
    }
    where the fields are accessible as kwargs, e.g kwargs['Id']

    Meant to react to messages from UBW with routing key "arbeidsordre".
    """

    # Ensure we have access to the necessary clients
    ensure_context(self)
    ctx = self.context

    # Ignore if delete operation
    # Replace with logic for removal if ompostering api supports it in
    # the future
    if kwargs.get("Operation") == "delete":
        logger.info("Nothing to do for Operation delete, skipping")
        return None

    try:
        company_id = kwargs["Client"]
    except KeyError:
        raise ValueError("Client is missing or null")
    try:
        arbeidsordre_id = kwargs["Id"]
    except KeyError:
        raise ValueError("Id is missing or null")

    ubw_arbeidsordre = ctx.ubw.get_arbeidsordre(company_id, arbeidsordre_id)

    # Prevent posting empty work order
    if ubw_arbeidsordre is None:
        logger.info(
            "Work order %s not found for company %s", arbeidsordre_id, company_id
        )
        return None

    arbeidsordre = arbeidsordre_ubw_to_om(ubw_arbeidsordre)
    ctx.ompostering.post_arbeidsordre(arbeidsordre)
    return True


stateless_ensure_arbeidsordre = functools.partial(
    ensure_arbeidsordre, SimpleNamespace()
)


def ensure_kontoplan(
    self: Union[SimpleNamespace, celery.app.task.Task], *args: str, **kwargs: Any
) -> Optional[bool]:
    """
    Keep account info updated.

    The handler expects a payload with at least these fields:
    {
        "Client":"72",
    }
    where the fields are accessibles as kwargs, e.g kwargs['Client']

    Meant to react to messages from UBW with routing key "kontoplan".
    """

    # Ensure we have access to the necessary clients
    ensure_context(self)
    ctx = self.context

    try:
        company_id = kwargs["Client"]
    except KeyError:
        raise ValueError("Client is missing or null")

    kontoplan = ctx.ubw.get_kontoplan(company_id)
    plan = Kontoplan(company_id=kontoplan.company_id, accounts=kontoplan.accounts)
    ctx.ompostering.post_kontoplaner(plan.company_id, plan.accounts)
    return True


stateless_ensure_kontoplan = functools.partial(ensure_kontoplan, SimpleNamespace())


def ensure_user(
    self: Union[SimpleNamespace, celery.app.task.Task], *args: str, **kwargs: Any
) -> Optional[bool]:
    """
    Keep user info updated.

    The handler expects the following payload:
    {
        "Client":"9900",
        "Id":"10000126",
    }
    where the fields are accessibles as kwargs, e.g kwargs['Id']

    Meant to react to messages from UBW with routing key "brukere".
    """

    # Ensure we have access to the necessary clients
    ensure_context(self)
    ctx = self.context

    try:
        company_id = kwargs["Client"]
    except KeyError:
        raise ValueError("Client is missing or null")
    try:
        user_id = kwargs["Id"]
    except KeyError:
        raise ValueError("Id is missing or null")

    bruker = ctx.ubw.get_bruker(company_id, user_id)
    bruker = Bruker(**bruker.dict())
    ctx.ompostering.post_bruker(company_id, bruker)
    return True


stateless_ensure_user = functools.partial(ensure_user, SimpleNamespace())


def ensure_avgiftskoder(
    self: Union[SimpleNamespace, celery.app.task.Task], *args: str, **kwargs: Any
) -> Optional[bool]:
    """
    Keep avgiftskoder info updated.

    The handler expects the following payload:
    {
        "Client": "72",
    }
    where the fields are accessibles as kwargs, e.g kwargs['Client']

    Meant to react to messages from UBW with routing key "avgiftskoder".
    """

    # Ensure we have access to the necessary clients
    ensure_context(self)
    ctx = self.context

    try:
        company_id = kwargs["Client"]
    except KeyError:
        raise ValueError("Client is missing or null")

    ubw_avgiftskoder = ctx.ubw.get_avgiftskode(company_id)
    omp_avgiftskoder = [Avgiftskode(**x.dict()) for x in ubw_avgiftskoder]
    ctx.ompostering.post_avgiftskoder(company_id, omp_avgiftskoder)
    return True


stateless_ensure_avgiftskoder = functools.partial(
    ensure_avgiftskoder, SimpleNamespace()
)


def ensure_ansattnr(
    self: Union[SimpleNamespace, celery.app.task.Task], *args: str, **kwargs: Any
) -> bool:
    """
    Keep ansattnr updated

    We are listening for changes on ressurs endpoint in ubw.
    When changed we use begrep endpoint to update ompostering

    The handler expects the following payload:
    {
        "Client":"9900",
        "Id":"10000126",
    }
    where the fields are accessibles as kwargs, e.g kwargs['Id']

    Meant to react to messages from UBW with routing key "ressurser".
    """
    # Ensure we have access to the necessary clients
    ensure_context(self)
    ctx = self.context

    try:
        company_id = kwargs["Client"]
    except KeyError:
        raise ValueError("Client is missing or null")
    try:
        ressurs_id = kwargs["Id"]
    except KeyError:
        raise ValueError("Id is missing or null")

    errors = []
    ubw_ansattnr = ctx.ubw.get_begrepsverdier(company_id, "C0", begrep_verdi=ressurs_id)
    omp_ready = [Begrepsverdi(**x.dict()) for x in ubw_ansattnr]
    for value in omp_ready:
        try:
            ctx.ompostering.post_begrepsverdi(value)
        except HTTPError:
            json = value.json(by_alias=True)
            errors.append(json)
    if len(errors) > 0:
        raise ValueError("Error posting %s values: %s" % (len(errors), errors))
    return True


stateless_ensure_ansattnr = functools.partial(ensure_ansattnr, SimpleNamespace())


def ensure_bilagsart(
    self: Union[SimpleNamespace, celery.app.task.Task], *args: str, **kwargs: Any
) -> bool:
    """
    Keep bilagsart updated

    We are listening for changes on bilagsart endpoint in ubw.
    When changed we use bilagstype endpoint to update ompostering

    The handler expects the following payload:
    {
        "Client":"72",
        "Id":"100008100",
        "LastUpdate":"2020-11-04 14.39.19",
        "Url":"dfo-devtest.azure-api.net/bilagsart/v1/72/{id}",
        "Operation":"update",
        "OperationOrder":"2"
    }
    where the fields are accessibles as kwargs, e.g kwargs['Client']

    TODO: Decide if this handler should be removed or if we should start listening to
     something that triggers it. If so, update the deployment config and remove this
     comment. Update below with correct routing key as well.
    Meant to react to messages from UBW with routing key "<insert routingkey>".
    """
    # Ensure we have access to the necessary clients
    ensure_context(self)
    ctx = self.context

    try:
        company_id = kwargs["Client"]
    except KeyError:
        raise ValueError("Client is missing or null")
    errors = []
    ubw_bilagsart = ctx.ubw.get_bilagstyper(company_id)
    omp_ready = [Bilagstype(**x.dict()) for x in ubw_bilagsart]
    for value in omp_ready:
        try:
            ctx.ompostering.post_bilagstype(value)
        except HTTPError:
            json = value.json(by_alias=True)
            errors.append(json)
    if len(errors) > 0:
        raise ValueError("Error posting %s values: %s" % (len(errors), errors))
    return True


stateless_ensure_bilagsart = functools.partial(ensure_bilagsart, SimpleNamespace())


def _batch_id_key_generator(*args: str, **kwargs: Any) -> str:
    batch_id = kwargs["identifier"]
    return f"lock-batch-id-{batch_id}"


def lock_dsn_resolver(*args: str, **kwargs: Any) -> str:
    return get_config().lock_dsn


@locker(key_generator=_batch_id_key_generator, dsn_resolver=lock_dsn_resolver)
def ensure_transaction_status(
    self: Union[SimpleNamespace, celery.app.task.Task], *args: str, **kwargs: Any
) -> Union[bool, JsonType]:
    """
    Reacts on messages that notify about the changes of status for a given batch.

    The handler expects the setra_transform transformation to be used on the event so
    that the payload has at least the following keys
    {
        "identifier":"252",
    }
    where the fields are accessible as kwargs, e.g kwargs['identifier']

    Meant to react to messages from SETRA with routing keys
        - 'setra.institution.env.OP.created'
        - 'setra.institution.env.OP.validation_completed'
        - 'setra.institution.env.OP.sent_to_ubw'
        - 'setra.institution.env.OP.polling_completed'
        - 'setra.institution.env.OP.ubw_import_ok'
        - 'setra.institution.env.OP.response_fetched_from_ubw'
        - 'setra.institution.env.OP..*_failed'

    Locking is added to ensure that one status is handled at time for the given batch id
    """
    ensure_context(self)
    ctx = self.context
    batch_id = kwargs["identifier"]

    batch_data = ctx.setra.get_batch_complete(batch_id)
    error_data = ctx.setra.get_batch_errors(batch_id)
    real_batch = CompleteBatch(**batch_data)
    batch_obj = _complete_batch_to_ompostering_batch(batch_id, real_batch, error_data)
    ctx.ompostering.post_setra_data(batch_obj)
    if real_batch.getresult_report:
        ctx.ompostering.post_ubw_report(real_batch, real_batch.getresult_report)
    return True


stateless_ensure_transaction_status = functools.partial(
    ensure_transaction_status, SimpleNamespace()
)
