import logging.config
from typing import Any
from ompostering_ms.config import OmposteringMsConfigLoader

logger = logging.getLogger(__name__)


def configure_sentry(sentry_config: dict[str, Any]) -> None:
    from sentry_sdk import init

    dsn = sentry_config.get("dsn")
    if not dsn:
        raise Exception("Missing Sentry DSN")
    init(
        dsn,
        environment=sentry_config.get("environment"),
        release=sentry_config.get("release"),
    )
    logger.info("Configured logging for sentry")


def setup_logging(config_loader: OmposteringMsConfigLoader) -> None:
    """Setup logging format and sentry"""
    if config_loader.logging is None:
        logger.warning("Logging config is missing")
        return

    logging.config.dictConfig(config_loader.logging)

    if config_loader.sentry:
        configure_sentry(config_loader.sentry)
