import cx_Oracle  # type: ignore
import time
import sys
import logging
from ompostering_ms.config import get_config
import json
from typing import Any, Callable, Dict
from pika_context_manager import PCM

config = get_config()
logger = logging.getLogger(__name__)
level = logging.getLevelName(config.cqn.logging_level)
logger.setLevel(level)
console = logging.StreamHandler(sys.stdout)
logger.addHandler(console)

JsonType = Any


def send_message(batchid: str, firma: str) -> None:
    # Send notifications to rabbit
    message = json.dumps({"batchid": batchid, "firma": firma})
    with PCM(**dict(config.notification_publisher.mq.connection)) as pcm:
        if pcm.publish(
            config.notification_publisher.exchange,
            routing_key=config.notification_publisher.routing_key,
            message=message,
            content_type="application/json",
        ):
            logger.info(
                "Published %s with routing key %s to exchange %s",
                message,
                config.notification_publisher.routing_key,
                config.notification_publisher.exchange,
            )
        else:
            logger.error(
                "Failed to publish %s with routing key %s to exchange %s",
                message,
                config.notification_publisher.routing_key,
                config.notification_publisher.exchange,
            )


registered = True
connection = None


def callback(message: JsonType) -> None:
    # This procedure is called every time the oracle-query has a changed result set
    global registered
    if not message.registered:
        logger.warning("Deregistration has taken place...")
        registered = False
        return
    if not connection:
        return
    for query in message.queries:
        for table in query.tables:
            if table.rows is not None:
                for row in table.rows:
                    if row.operation & cx_Oracle.OPCODE_INSERT:
                        cur = connection.cursor()
                        #  TODO: Bind these things properly instead of making sql injection possible
                        cur.execute(
                            f"select batch_id, firma from ompostering.omp_hode_hist where rowid='{row.rowid}'"
                            f" or (opprettet<sysdate-'{config.cqn.pick_up_old_batches_time_delay}'/(60*24) "
                            f" and resultat is null) "
                        )
                        batches = cur.fetchall()
                        for batch in batches:
                            batchid, firma = batch
                            logger.info("batchid: %s firma: %s", batchid, firma)
                            send_message(batchid, firma)
                        cur.close()


def main() -> None:
    dsn = cx_Oracle.makedsn(config.cqn.server, config.cqn.port, config.cqn.sid)
    global connection
    connection = cx_Oracle.connect(
        user=config.cqn.user, password=config.cqn.password, dsn=dsn, events=True
    )

    subscription = connection.subscribe(
        callback=callback,
        timeout=config.cqn.timeout,
        port=config.cqn.callback_port,
        operations=cx_Oracle.OPCODE_INSERT,
        qos=cx_Oracle.SUBSCR_QOS_QUERY | cx_Oracle.SUBSCR_QOS_ROWIDS,
    )

    query_id = subscription.registerquery(
        "select batch_id, firma from ompostering.omp_hode_hist"
    )
    logger.info("Registered query: %s", query_id)

    # On startup : check if some batches have not been sent
    cur = connection.cursor()
    #  TODO: Bind these things properly instead of making sql injection possible
    cur.execute(
        f"select batch_id, firma from ompostering.omp_hode_hist where "
        f" (opprettet<sysdate-'{config.cqn.pick_up_old_batches_time_delay}'/(60*24) "
        f" and resultat is null) "
    )
    batches = cur.fetchall()
    for batch in batches:
        batchid, firma = batch
        logger.info("batchid: %s firma: %s", batchid, firma)
        send_message(batchid, firma)
    cur.close()

    while registered:
        logger.debug(
            f'Waiting for notifications....{time.strftime("%H:%M:%S", time.localtime())}'
        )
        time.sleep(config.cqn.sleeping_time)


if __name__ == "__main__":
    main()
