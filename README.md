# ompostering-ms

Microservice for delivering transcations from Omposteringsløsningen to SETRA

## Installation

[Poetry v1.2+](https://python-poetry.org/docs/) is used for dependency management.

    # Make sure poetry is installed, see docs
    poetry shell        # Start a shell, creating a virtual environment.
    poetry install      # Install dependencies from lock file

Activate [pre-commit](https://pre-commit.com/) hooks:

    pre-commit install

## Local configuration

Local configuration is read from `config.yaml` or from the file specified
in the environment variable `OMPOSTERING_MS_CONFIG`.

    $ cp config.example.yaml config.yaml
    # Edit the example config file

## Full provisioning

```sh
poetry run python -m provision
```

Add flag `-h` to see usage and options.

## Testing

    poetry run pytest

Or run integration tests with

    poetry run pytest -m integration

### Coverage

```sh
poetry run coverage run --branch --source=ompostering_ms,provision -m pytest && poetry run coverage report --show-missing
```

## Receiving and processing notifications

The TOFH framework will pluck messages off a message queue and forward to Celery.
Both must be started for successful operation.

NB! The commands below are for starting each of TOFH and Celery as foreground processes,
typically each in their own terminal window. They don't detach.

Start TOFH:

    TOFH_CONFIG=config.yaml OMPOSTERING_MS_CONFIG=config.yaml python -m tofh.plugins.mq

Start Celery:

    TOFH_CONFIG=config.yaml OMPOSTERING_MS_CONFIG=config.yaml celery worker --task-events --app=tofh.app.create --loglevel=DEBUG

## Integration-testing using RabbitMQ

- Update `config.yaml` with your RabbitMQ `username`, `password` and `your-vhost` (found when logged in to RabbitMQ)
- Go to https://mq-mgmt-uio.green-apps.intark.uh-it.no/#/exchanges
- Create a new exchange named `ex_ompostering_ms`, with type `topic`
- Use this exchange to send a message with the correct routing key and payload
  - Example:  
    Routing key: `ensure_projects_updated`  
    Payload: `{"id":"110001","firmakode":"72"}`

## Static Type Analysis

Use [mypy](http://mypy-lang.org/) to run static type checks using type hints.

    mypy .

## Docker

A `Dockerfile` is provided for application deployment purposes. It is not meant to be used for development.

`update-harbor-image.sh` is a utility script for building and uploading a Docker image to our private image repository `harbor.uio.no`.

It expects you to be logged in:

    docker login harbor.uio.no

## Deployment

See the [deploy-bott-int repository](https://bitbucket.usit.uio.no/projects/BOTTINT/repos/deploy-bott-int).

## Starting the Ompostering notification publisher

The publisher must be started as a standalone program:

    python ompostering_ms/ompostering-publisher.py

It will then listen to notifications from oracle (i.e. new batches ready to be
transferred to SETRA), and send notifications to Rabbit.
