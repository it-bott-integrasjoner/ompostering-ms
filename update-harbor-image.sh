#!/bin/sh
set -x

export DOCKER_BUILDKIT=1
# Make sure podman builds images compatible with OpenShift
export BUILDAH_FORMAT=docker
IMAGE_PREFIX=${IMAGE_PREFIX:-harbor.uio.no/bott-int}

slugify() {
  # Turn all non-ascii non-alphanumeric characters to -, then lowercase.
  # e.g. 'feature/FOO-123_blæh_blah' -> 'feature-foo-123-bl-h-blah'
  printf %s "$1" |
    LC_ALL=C sed -E 's/[^[:alnum:]]+/-/g' |
    tr '[:upper:]' '[:lower:]'
}

# Handle running the script locally, and on gitlab (otherwise, gitlab would get head as the branch name):
if [ -n "$CI_COMMIT_REF_NAME" ]; then
  git_branch=$CI_COMMIT_REF_NAME
  git_sha=$CI_COMMIT_SHORT_SHA
else
  git_branch=$(git rev-parse --abbrev-ref HEAD) &&
  git_sha=$(git rev-parse --short HEAD) || exit
fi

app_name=ompostering-ms
container=$IMAGE_PREFIX/$app_name
image_tag=$container:$(slugify "$git_branch")-$git_sha

if command -v podman > /dev/null 2>&1; then
  docker() { podman "$@"; }
  printf 'Will build using podman\n'
elif command -v docker > /dev/null 2>&1; then
  printf 'Will build using docker\n'
else
  printf >&2 'Missing podman or docker CLI tools\n'
  exit 1
fi

printf 'Generating .dockerignore\n'
git ls-files | awk 'BEGIN{print ".*\n*"} {print "!"$0}' > .dockerignore

printf 'Building %s\n' "$image_tag"
docker build -t "$image_tag" . --no-cache|| exit

printf 'Pushing %s\n' "$image_tag"
docker push "$image_tag" || exit

if [ "$git_branch" = master ]; then
  printf 'On master branch, setting %s as %s:latest\n' "$image_tag" "$container"
  docker tag "$image_tag" "$container:latest"
  docker push "$container:latest"
fi

if [ "$git_branch" = production ]; then
  printf 'On production branch, setting %s as %s:production\n' "$image_tag" "$container"
  docker tag "$image_tag" "$container:production"
  docker push "$container:production"
fi

# Tag with git tag if present
vers_tag=${CI_COMMIT_TAG:-$(git tag --sort=-taggerdate --points-at=HEAD | sed 1q)}

if [ -n "$vers_tag" ]; then
  # push tagged version
  printf 'Setting %s as %s:%s\n' "$image_tag" "$container" "$vers_tag"
  docker tag "$image_tag" "$container:$vers_tag"
  docker push "$container:$vers_tag"
  printf "Release version: <%s>" "$vers_tag"
fi
