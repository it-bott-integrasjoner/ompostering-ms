import json
import os
import pathlib

import pytest

from ompostering_ms.config import OmposteringMsConfigLoader
from ompostering_ms.context import OmposteringMsContext


@pytest.fixture
def ompostering_ms_config(config: OmposteringMsConfigLoader):
    return config.ompostering_ms


@pytest.fixture
def config_example_path():
    return pathlib.Path(__file__).parent.parent.joinpath("config.example.yaml")


@pytest.fixture
def config_test_path():
    return pathlib.Path(__file__).parent.joinpath("fixtures", "testconfig.yaml")


@pytest.fixture
def config(config_test_path):
    loader = OmposteringMsConfigLoader.from_file(config_test_path)
    assert loader.sentry
    return loader


def load_json_file(name):
    here = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
    with open(os.path.join(here, "fixtures", name)) as f:
        data = json.load(f)
    return data


@pytest.fixture
def ompostering_ms_context(config):
    ctx = OmposteringMsContext(config_loader=config)
    return ctx


@pytest.fixture
def anlegg():
    return load_json_file("anlegg.json")


@pytest.fixture
def anlegg_10000011():
    return load_json_file("anlegg_10000011.json")


@pytest.fixture
def project():
    return load_json_file("project.json")


@pytest.fixture
def projects():
    return load_json_file("projects.json")


@pytest.fixture
def company():
    return load_json_file("company.json")


@pytest.fixture
def attribute_values():
    return load_json_file("attribute_values.json")


@pytest.fixture
def attribute_values_projects():
    return load_json_file("attribute_values_projects.json")


@pytest.fixture
def attribute_values_arbeidsordrer():
    return load_json_file("attribute_values_arbeidsordrer.json")


@pytest.fixture
def period():
    return load_json_file("period.json")


@pytest.fixture
def arbeidsordre():
    return load_json_file("arbeidsordre.json")


@pytest.fixture
def arbeidsordrer():
    return load_json_file("arbeidsordrer.json")


@pytest.fixture
def badbruker():
    return load_json_file("badbruker.json")


@pytest.fixture
def bruker():
    return load_json_file("bruker.json")


@pytest.fixture
def ressurs():
    return load_json_file("ressurs.json")


@pytest.fixture
def attribute_values_ansattnr():
    return load_json_file("attribute_values_ansattnr.json")


@pytest.fixture
def mq_msg_update_ressurs():
    return load_json_file("mq_msg_update_ressurs.json")


@pytest.fixture
def kontoplan_72():
    return load_json_file("kontoplan_72.json")


@pytest.fixture
def koststeder():
    return load_json_file("koststeder.json")


@pytest.fixture
def avgiftskoder():
    return load_json_file("avgiftskoder.json")


@pytest.fixture
def bilagstype():
    return load_json_file("bilagstype.json")


@pytest.fixture
def attribute_values_bilagsart():
    return load_json_file("attribute_values_bilagsart.json")


@pytest.fixture
def batch_complete():
    return load_json_file("batch_complete.json")


@pytest.fixture
def batch_errors():
    return load_json_file("batch_errors.json")


@pytest.fixture
def mq_msg_update_bilagstype():
    return load_json_file("mq_msg_update_bilagstype.json")


@pytest.fixture
def mq_msg_update_transaction_status():
    return load_json_file("mq_msg_update_transaction_status.json")


@pytest.fixture
def mq_ensure_transaction_setra():
    return load_json_file("mq_ensure_transaction_setra.json")


@pytest.fixture
def transaksjon_hode():
    return load_json_file("transaksjon_hode.json")


@pytest.fixture
def transaksjon_linje():
    return load_json_file("transaksjon_linje.json")


@pytest.fixture
def transaksjon_linje_tom_beskrivelse():
    return load_json_file("transaksjon_linje_tom_beskrivelse.json")


@pytest.fixture
def omp_hode():
    return load_json_file("omp_hode.json")


@pytest.fixture
def setra_addtrans_response():
    return load_json_file("setra_addtrans_response.json")


def pytest_collection_modifyitems(config, items):
    if config.option.keyword or config.option.markexpr:
        return
    skip_integration = pytest.mark.skip(
        reason='Not running with pytest -m "integration"'
    )
    for item in items:
        if "integration" in item.keywords:
            item.add_marker(skip_integration)
