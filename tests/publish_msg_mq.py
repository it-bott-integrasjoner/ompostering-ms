import os

import yaml
from pika_context_manager import PCM

routing_key = "setra.uio.staging.OP.created"


def publish_msg_to_rabbit_mq(msg_count=1):
    """Test file to send msg to the rabbit mq for given host and exchange"""
    env = "test"
    interface = "uio"
    config = get_config_dict()

    run = config["rabbit_mq_test"][interface][env]
    print("Run config, vhost", str(run["vhost"]))
    with PCM(
        hostname=config["rabbit_mq_test"][interface]["host"],
        port=5671,
        virtual_host=run["vhost"],
        username=run["user"],
        password=run["password"],
        tls_on=True,
    ) as pcm:

        print("Publishing msg: ")

        for i in range(0, 10):
            msg = f""" "specversion": "1.0", "id": "44398", "source": "urn:setra:uio:staging:batch:6495", "type": "setra.uio.staging.OP.ubw_import_ok", "identifier": "6495" """

            published = pcm.publish(
                run["queue"],
                routing_key,
                "{" + msg + "}",
            )
            print(i, ") status: ", published, "Published, msg: " + msg)

        print("Finished, sent msg to queue")


def get_config_dict():
    current_path = os.path.realpath(
        os.path.join(os.getcwd(), os.path.dirname(__file__))
    )
    config_path = os.path.join(current_path, "..", "config.yaml")
    with open(config_path, "r") as f:
        config = yaml.load(f.read(), Loader=yaml.FullLoader)
    return config


if __name__ == "__main__":
    publish_msg_to_rabbit_mq()
