from ompostering_ms.config import RetryConfig


def do_test_session_http_adapter(client_session, ompostering_ms_context):
    http_adapter = client_session.get_adapter("http://example.com")
    https_adapter = client_session.get_adapter("https://example.com")
    for prop in RetryConfig.__fields__:
        configured = getattr(ompostering_ms_context.config.http_retry, prop)
        assert configured == getattr(http_adapter.max_retries, prop)
        assert configured == getattr(https_adapter.max_retries, prop)


def test_ubw_client_session_http_adapter(ompostering_ms_context):
    client_session = ompostering_ms_context.ubw.rest_session

    do_test_session_http_adapter(client_session, ompostering_ms_context)


def test_setra_client_session_http_adapter(ompostering_ms_context):
    client_session = ompostering_ms_context.setra.session

    do_test_session_http_adapter(client_session, ompostering_ms_context)


def test_ompostering_client_session_http_adapter(ompostering_ms_context):
    client_session = ompostering_ms_context.ompostering.session

    do_test_session_http_adapter(client_session, ompostering_ms_context)
