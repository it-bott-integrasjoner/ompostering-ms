from ompostering_ms.config import (
    OmposteringMsConfig,
    OmposteringMsConfigLoader,
    get_config,
)


def test_config_loader(config_example_path):
    with open(config_example_path) as f:
        example_config_yamlstr = f.read()
    config_from_yamlstr = OmposteringMsConfigLoader.from_yaml(example_config_yamlstr)
    config_from_file = OmposteringMsConfigLoader.from_file(config_example_path)
    config_from_file.filename = None
    assert config_from_file == config_from_yamlstr
    assert isinstance(config_from_file, OmposteringMsConfigLoader)
    assert isinstance(config_from_file.ompostering_ms, OmposteringMsConfig)


def test_get_config(config_example_path):
    """The example config is okay"""
    config = get_config(filename=config_example_path)
    assert isinstance(config, OmposteringMsConfig)
