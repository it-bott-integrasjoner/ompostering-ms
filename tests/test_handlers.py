import json
import re
from types import SimpleNamespace
import importlib
from functools import wraps
from unittest.mock import patch
import pytest
from setra_client.models import ResponseStatusEnum
from ubw_client.models import (
    Arbeidsordre,
    Bruker,
    Firma,
    Prosjekt,
)
from ubw_client.client import IncorrectPathError
from ompostering_ms.handlers import (
    ensure_anlegg,
    ensure_arbeidsordre,
    ensure_attribute_values,
    ensure_company,
    ensure_koststed,
    ensure_period,
    ensure_project,
    ensure_user,
    ensure_avgiftskoder,
    ensure_ansattnr,
    ensure_bilagsart,
    ensure_transaksjon_to_setra,
    _is_double_submission_to_setra,
)


def test_ensure_anlegg(ompostering_ms_context, anlegg_10000011, requests_mock):
    o = SimpleNamespace()
    o.context = ompostering_ms_context
    requests_mock.get(
        "https://example.com/ubw/anlegg/72/10000011",
        text=json.dumps(anlegg_10000011),
    )
    requests_mock.post("https://example.com/ompostering/begrepsverdier_api/", text="{}")
    # Check correct works
    assert ensure_anlegg(o, Id="10000011", Client="72") is None
    # Check missing kwargs fail
    with pytest.raises(ValueError):
        ensure_anlegg(o)
    with pytest.raises(ValueError):
        ensure_anlegg(o, Client="72")
    with pytest.raises(ValueError):
        ensure_anlegg(o, Id="72")


def test_ensure_koststed(ompostering_ms_context, koststeder, requests_mock):
    o = SimpleNamespace()
    o.context = ompostering_ms_context

    company_id = "72"
    anlegg_id = "20101030"
    requests_mock.get(
        f"https://example.com/ubw/koststeder/v1/{company_id}/{anlegg_id}",
        text=json.dumps(koststeder[:1]),
    )
    requests_mock.post("https://example.com/ompostering/begrepsverdier_api/", text="{}")
    assert ensure_koststed(o, Id="20101030", Client="72") is None
    # Check missing kwargs fail
    with pytest.raises(ValueError):
        ensure_koststed(o)
    with pytest.raises(ValueError):
        ensure_koststed(o, Client="72")
    with pytest.raises(ValueError):
        ensure_koststed(o, Id="72")


def test_ensure_project(ompostering_ms_context, project, requests_mock):
    o = SimpleNamespace()
    o.context = ompostering_ms_context

    requests_mock.get(
        "https://example.com/ubw/prosjekter/72/110001",
        text=Prosjekt(**project).json(),
    )
    requests_mock.post("https://example.com/ompostering/prosjekt_api/", text="{}")
    assert ensure_project(o, Id="110001", Client="72")


def test_ensure_company(ompostering_ms_context, company, requests_mock):
    o = SimpleNamespace()
    o.context = ompostering_ms_context

    requests_mock.get("https://example.com/ubw/firma/72", text=Firma(**company).json())
    requests_mock.post("https://example.com/ompostering/firma_api/", text="{}")
    assert ensure_company(o, Client="72")


def test_ensure_attribute_values(
    ompostering_ms_context, attribute_values, requests_mock
):
    o = SimpleNamespace()
    o.context = ompostering_ms_context

    requests_mock.get(
        "https://example.com/ubw/begrepsverdier/72/B0",
        text=json.dumps(attribute_values),
    )
    requests_mock.post("https://example.com/ompostering/begrepsverdier_api/", text="{}")
    assert ensure_attribute_values(o, Client="72", AttributeId="B0", Id="A")


def test_ensure_single_attribute_value(
    ompostering_ms_context, attribute_values, requests_mock
):
    o = SimpleNamespace()
    o.context = ompostering_ms_context

    attribute_value = "100007"
    requests_mock.get(
        "https://example.com/ubw/begrepsverdier/72/B0"
        f"?filter=attributeValue+eq+%27{attribute_value}%27",
        text=json.dumps(attribute_values),
    )
    requests_mock.post("https://example.com/ompostering/begrepsverdier_api/", text="{}")
    assert ensure_attribute_values(o, Client="72", AttributeId="B0", Id=attribute_value)


def test_ensure_period(ompostering_ms_context, period, requests_mock):
    o = SimpleNamespace()
    o.context = ompostering_ms_context

    requests_mock.get(
        "https://example.com/ubw/perioder/72/202001", text=json.dumps(period)
    )
    requests_mock.post("https://example.com/ompostering/perioder_api/", text="{}")
    assert ensure_period(o, Client="72", Id="202001")


def test_ensure_arbeidsordre(ompostering_ms_context, arbeidsordre, requests_mock):
    o = SimpleNamespace()
    o.context = ompostering_ms_context

    requests_mock.get(
        "https://example.com/ubw/arbeidsordre/72/110000100",
        text=Arbeidsordre(**arbeidsordre).json(),
    )
    requests_mock.post("https://example.com/ompostering/arbeidsordre_api/", text="{}")
    assert ensure_arbeidsordre(o, Client="72", Id="110000100")


def test_ensure_user(ompostering_ms_context, bruker, requests_mock):
    o = SimpleNamespace()
    o.context = ompostering_ms_context

    requests_mock.get(
        "https://example.com/ubw/brukere/72/5",
        text=Bruker(**bruker).json(),
    )
    requests_mock.post("https://example.com/ompostering/bruker_api/72", text="{}")
    assert ensure_user(o, Id="5", Client="72")


def test_ensure_avgiftskoder(ompostering_ms_context, avgiftskoder, requests_mock):
    o = SimpleNamespace()
    o.context = ompostering_ms_context

    requests_mock.get(
        "https://example.com/ubw/avgiftskoder/72",
        text=json.dumps(avgiftskoder),
    )
    requests_mock.post("https://example.com/ompostering/avgiftskoder_api/72", text="{}")
    assert ensure_avgiftskoder(o, Client="72")


def test_ensure_ansattnr(
    ompostering_ms_context,
    attribute_values_ansattnr,
    mq_msg_update_ressurs,
    requests_mock,
):
    o = SimpleNamespace()
    o.context = ompostering_ms_context

    requests_mock.get(
        "https://example.com/ubw/begrepsverdier/72/C0",
        text=json.dumps(attribute_values_ansattnr),
    )
    requests_mock.post("https://example.com/ompostering/begrepsverdier_api/", text="{}")

    assert ensure_ansattnr(o, **mq_msg_update_ressurs)


def test_ensure_bilagstyper(
    ompostering_ms_context, bilagstype, mq_msg_update_bilagstype, requests_mock
):
    o = SimpleNamespace()
    o.context = ompostering_ms_context

    requests_mock.get(
        "https://example.com/ubw/bilagstyper/72",
        text=json.dumps(bilagstype),
    )
    requests_mock.post("https://example.com/ompostering/bilagstyper_api/", text="{}")

    assert ensure_bilagsart(o, **mq_msg_update_bilagstype)


def test_ensure_transaction_status(
    ompostering_ms_context,
    batch_complete,
    batch_errors,
    mq_msg_update_transaction_status,
    requests_mock,
):

    o = SimpleNamespace()
    o.context = ompostering_ms_context

    requests_mock.get(
        "https://example.com/setra/api/batch_complete/252",
        text=json.dumps(batch_complete),
    )
    requests_mock.get(
        "https://example.com/setra/api/batch_error/252", text=json.dumps(batch_errors)
    )
    requests_mock.post("https://example.com/ompostering/omp_hode_api/72/252", text="{}")

    with patch("lock_context_manager.locker", mock_decorator):
        from ompostering_ms import handlers

        importlib.reload(
            handlers
        )  # reload the handler to remove already loaded locker and load with mock_decorator
        from ompostering_ms.handlers import ensure_transaction_status

        assert ensure_transaction_status(o, **mq_msg_update_transaction_status)


def test_ensure_transaksjon_to_setra(
    ompostering_ms_context,
    mq_ensure_transaction_setra,
    requests_mock,
    transaksjon_linje,
    transaksjon_hode,
    omp_hode,
    setra_addtrans_response,
):
    requests_mock.get(
        "https://example.com/ompostering/transaksjon_hode_url/75/123",
        json=transaksjon_hode,
    )

    requests_mock.get(
        "https://example.com/ompostering/transaksjon_linje_url/75/123?limit=10000",
        json=transaksjon_linje,
    )
    requests_mock.get(
        "https://example.com/ompostering/omp_hode_api/75/123",
        json=omp_hode,
    )
    requests_mock.post(
        "https://example.com/setra/api/addtrans/",
        status_code=202,
        json=setra_addtrans_response,
    )
    o = SimpleNamespace()
    o.context = ompostering_ms_context
    ensure_transaksjon_to_setra(o, **mq_ensure_transaction_setra)

    def test_non_success_response(response_status):
        if response_status == ResponseStatusEnum.CONFLICT:
            status_code = 409
        elif response_status == ResponseStatusEnum.UNKNOWN:
            status_code = 200
        else:
            raise ValueError(str(response_status))
        requests_mock.post(
            "https://example.com/setra/api/addtrans/",
            status_code=status_code,
            json=setra_addtrans_response,
        )
        with pytest.raises(ValueError) as exc_info:
            ensure_transaksjon_to_setra(o, **mq_ensure_transaction_setra)
        resp = {"code": status_code, "content": setra_addtrans_response}
        err_msg = f"Unexpected status: {response_status.value}. Response: {resp}"
        exc_info.match(f"^{re.escape(err_msg)}$")

    test_non_success_response(ResponseStatusEnum.CONFLICT)
    test_non_success_response(ResponseStatusEnum.UNKNOWN)


def test_ensure_transaksjon_to_setra_empty_transaksjon_hode(
    ompostering_ms_context,
    mq_ensure_transaction_setra,
    requests_mock,
):
    requests_mock.get(
        "https://example.com/ompostering/transaksjon_hode_url/75/123",
        json={"items": []},
    )
    # Should not raise an error
    ensure_transaksjon_to_setra(
        SimpleNamespace(context=ompostering_ms_context),
        **mq_ensure_transaction_setra,
    )


def test_ensure_transaksjon_to_setra_empty_description(
    ompostering_ms_context,
    mq_ensure_transaction_setra,
    requests_mock,
    transaksjon_linje_tom_beskrivelse,
    transaksjon_hode,
    omp_hode,
    setra_addtrans_response,
):
    requests_mock.get(
        "https://example.com/ompostering/transaksjon_hode_url/75/123",
        json=transaksjon_hode,
    )

    requests_mock.get(
        "https://example.com/ompostering/transaksjon_linje_url/75/123?limit=10000",
        json=transaksjon_linje_tom_beskrivelse,
    )
    requests_mock.get(
        "https://example.com/ompostering/omp_hode_api/75/123",
        json=omp_hode,
    )

    o = SimpleNamespace()
    o.context = ompostering_ms_context
    msg = (
        "Description field in a transaction for batchid: 123, firma: 75 is empty "
        + "which is forbidden. Cannot send batch further."
    )
    with pytest.raises(ValueError, match=msg):
        ensure_transaksjon_to_setra(o, **mq_ensure_transaction_setra)


def mock_decorator(*args, **kwargs):
    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            return f(*args, **kwargs)

        return decorated_function

    return decorator


def test_ensure_arbeidsordre_error_if_no_content(
    ompostering_ms_context, arbeidsordre, requests_mock
):
    o = SimpleNamespace()
    o.context = ompostering_ms_context

    requests_mock.get(
        "https://example.com/ubw/arbeidsordre/72/110000100",
        status_code=404,
    )
    with pytest.raises(IncorrectPathError):
        assert ensure_arbeidsordre(o, Client="72", Id="110000100")


def test_is_double_submission_to_setra():
    status1 = ResponseStatusEnum.UNKNOWN
    resp1 = {"code": 500, "content": "Internal Server Error"}

    status2 = ResponseStatusEnum.CONFLICT
    resp2 = {"code": 409, "content": "An irrelevant error message"}

    status3 = ResponseStatusEnum.CONFLICT
    resp3 = {
        "code": 409,
        "content": {
            "__all__": [["Bunt with this Firma, Avs system and Buntnr already exists."]]
        },
    }

    assert not _is_double_submission_to_setra(status1, resp1)
    assert not _is_double_submission_to_setra(status2, resp2)
    assert _is_double_submission_to_setra(status3, resp3)
