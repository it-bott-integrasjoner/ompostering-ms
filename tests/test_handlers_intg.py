import os
from types import SimpleNamespace

import pytest
from ompostering_ms.config import OmposteringMsConfigLoader
from ompostering_ms.context import OmposteringMsContext
from ompostering_ms.handlers import (
    ensure_arbeidsordre,
    ensure_attribute_values,
    ensure_company,
    ensure_period,
    ensure_project,
    ensure_user,
    ensure_ansattnr,
)

"""
Integrations tests: Disabled by default
Requires config.yaml 
- Copy config.example.yaml to config.yaml
- Set your RabbitMQ `username`, `password` and `your-vhost` (found when logged in to RabbitMQ)
To run the tests, run with `pytest -m "integration"`
"""


@pytest.fixture
def ompostering_ms_context_intgr():
    current_path = os.path.realpath(
        os.path.join(os.getcwd(), os.path.dirname(__file__))
    )
    config_path = os.path.join(current_path, "..", "config.yaml")
    ctx = OmposteringMsContext(
        config_loader=OmposteringMsConfigLoader.from_file(config_path)
    )
    return ctx


@pytest.mark.integration
def test_ensure_project(ompostering_ms_context_intgr):
    o = SimpleNamespace()
    o.context = ompostering_ms_context_intgr
    assert ensure_project(o, id="110001", firmakode="72")


@pytest.mark.integration
def test_ensure_company(ompostering_ms_context_intgr):
    o = SimpleNamespace()
    o.context = ompostering_ms_context_intgr

    assert ensure_company(o, firmakode="72")


@pytest.mark.integration
def test_ensure_attribute_values(ompostering_ms_context_intgr):
    o = SimpleNamespace()
    o.context = ompostering_ms_context_intgr

    assert ensure_attribute_values(o, firmakode="72", begrepId="AH")


@pytest.mark.integration
def test_ensure_single_attribute_value(ompostering_ms_context_intgr):
    o = SimpleNamespace()
    o.context = ompostering_ms_context_intgr

    assert ensure_attribute_values(o, firmakode="72", begrepId="AH", begrepVerdi="999")


@pytest.mark.integration
def test_ensure_single_attribute_value_long_description(ompostering_ms_context_intgr):
    o = SimpleNamespace()
    o.context = ompostering_ms_context_intgr

    assert ensure_attribute_values(o, firmakode="72", begrepId="AH", begrepVerdi="251")


@pytest.mark.integration
def test_ensure_period(ompostering_ms_context_intgr):
    o = SimpleNamespace()
    o.context = ompostering_ms_context_intgr

    assert ensure_period(o, firmakode="72", accountingPeriod="202001", periodType="GL")


@pytest.mark.integration
def test_ensure_arbeidsordre(ompostering_ms_context_intgr):
    o = SimpleNamespace()
    o.context = ompostering_ms_context_intgr

    assert ensure_arbeidsordre(o, firmakode="72", id="110000100")


@pytest.mark.integration
def test_ensure_user(ompostering_ms_context_intgr):
    o = SimpleNamespace()
    o.context = ompostering_ms_context_intgr

    assert ensure_user(o, firmakode="72", brukerId="0-joei")


@pytest.mark.integration
def test_ensure_ansattnr(ompostering_ms_context_intgr):
    o = SimpleNamespace()
    o.context = ompostering_ms_context_intgr

    assert ensure_ansattnr(o, Client="72", Id="99999")
