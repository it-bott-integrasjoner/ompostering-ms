"""Tests for the full provisioning CLI tool"""

import datetime
import json
import logging
import subprocess
import sys
import unittest.mock
import pathlib

import provision.__main__ as provision_main
import pytest
import requests


@pytest.fixture
def ubw_responses(
    anlegg,
    arbeidsordrer,
    attribute_values_arbeidsordrer,
    attribute_values_projects,
    badbruker,
    bruker,
    avgiftskoder,
    company,
    kontoplan_72,
    koststeder,
    period,
    projects,
    ressurs,
    bilagstype,
):
    return {
        "anlegg": anlegg,
        "arbeidsordrer": arbeidsordrer,
        "attribute_values_arbeidsordrer": attribute_values_arbeidsordrer,
        "attribute_values_projects": attribute_values_projects,
        "avgiftskoder": avgiftskoder,
        "brukere": [badbruker, bruker],
        "firma": company,
        "kontoplan_72": kontoplan_72,
        "koststeder": koststeder,
        "perioder": period,
        "prosjekt": projects,
        "ressurs": ressurs,
        "bilagstype": bilagstype,
    }


def _mock_urls(requests_mock, ubw_responses):
    """Mocks all URLs for full provisioning tests. Some tests may override
    these.
    """

    ubw_get_urls_responses_map = {
        "https://example.com/ubw/prosjekter/72": ubw_responses["prosjekt"],
        "https://example.com/ubw/firma/72": ubw_responses["firma"],
        (
            "https://example.com/ubw/perioder/72"
            "?filter=periodType%2FperiodType+eq+%27GL%27"
        ): ubw_responses["perioder"],
        "https://example.com/ubw/arbeidsordre/72": ubw_responses["arbeidsordrer"],
        "https://example.com/ubw/anlegg/72": ubw_responses["anlegg"],
        "https://example.com/ubw/avgiftskoder/72": ubw_responses["avgiftskoder"],
        "https://example.com/ubw/brukere/72": ubw_responses["brukere"],
        "https://example.com/ubw/kontoplan/v1/72": ubw_responses["kontoplan_72"],
        "https://example.com/ubw/koststeder/v1/72": ubw_responses["koststeder"],
        "https://example.com/ubw/ressurser/72": ubw_responses["ressurs"],
        "https://example.com/ubw/bilagstyper/72": ubw_responses["bilagstype"],
    }
    for url, response_data in ubw_get_urls_responses_map.items():
        requests_mock.get(
            url,
            text=json.dumps(response_data),
        )

    begrepsverdier_id_responses_map = {
        "A0": {},  # Konto (KONTO)
        "C1": {},  # Koststed (KOSTSTED)
        # Prosjekt (PROSJEKT)
        "B0": ubw_responses["attribute_values_projects"],
        # Delprosjekt (ARBORDRE)
        "BF": ubw_responses["attribute_values_arbeidsordrer"],
        "F0": {},  # Anlegg (ANLEGG)
        "C0": {},  # Ansattnummer (ANSATTNR)
        # TODO: "??": {},  # =Byggnr (_____)
        "B1": {},  # Arbeidspakke (AKTIVITET)
        "AX": {},  # Bilagsart (BIL.ART)
        "A1": {},  # Avgiftskode (AV)
        "AW": {},  # Regnskapsperiode (PERIODE)
    }
    for attribute_id, response in begrepsverdier_id_responses_map.items():
        requests_mock.get(
            f"https://example.com/ubw/begrepsverdier/72/{attribute_id}",
            text=json.dumps(response),
        )

    om_post_urls = [
        "https://example.com/ompostering/prosjekt_api/",
        "https://example.com/ompostering/prosjekt_batch_api/",
        "https://example.com/ompostering/firma_api/",
        "https://example.com/ompostering/perioder_api/",
        "https://example.com/ompostering/arbeidsordre_api/",
        "https://example.com/ompostering/arbeidsordre_batch_api/",
        "https://example.com/ompostering/bruker_api/72",
        "https://example.com/ompostering/brukere_batch_api/72",
        "https://example.com/ompostering/begrepsverdier_api/",
        "https://example.com/ompostering/kontoplaner_api/72",
        "https://example.com/ompostering/avgiftskoder_api/72",
        "https://example.com/ompostering/ressurs_api/",
        "https://example.com/ompostering/bilagstyper_api/",
        "https://example.com/ompostering/begrepsverdier_batch_api/",
    ]
    for url in om_post_urls:
        requests_mock.post(url, text="{}")


attribute_params = "attribute_name,ubw_url,om_url"
attribute_args = [
    (
        "firma",
        "https://example.com/ubw/firma/72",
        "https://example.com/ompostering/firma_api/",
    ),
    (
        "perioder",
        (
            "https://example.com/ubw/perioder/72"
            "?filter=periodType%2FperiodType+eq+%27GL%27"
        ),
        "https://example.com/ompostering/perioder_api/",
    ),
    (
        "prosjekt",
        "https://example.com/ubw/prosjekter/72",
        "https://example.com/ompostering/prosjekt_api/",
    ),
    (
        "arbeidsordrer",
        "https://example.com/ubw/arbeidsordre/72",
        "https://example.com/ompostering/arbeidsordre_api/",
    ),
    (
        "brukere",
        "https://example.com/ubw/brukere/72",
        "https://example.com/ompostering/bruker_api/",
    ),
    (
        "begrepsverdier",
        "https://example.com/ubw/begrepsverdier/72/B0",
        "https://example.com/ompostering/begrepsverdier_api/",
    ),
]


def _provision_no_cli(
    ctx,
    company_id: str,
    attribute: str,
    ignore_errors,
    assert_ubw_empty=False,
    assert_om_empty=False,
):
    """The provision CLI command, with all the CLI stuff stripped away."""

    instances_ubw = provision_main._fetch_from_ubw(
        ctx, [company_id], attribute, ignore_errors
    )
    for attribute, attribute_instances in instances_ubw.items():
        assert not attribute_instances if assert_ubw_empty else attribute_instances

    instances_ompostering = provision_main._convert_ubw_to_ompostering(
        instances_ubw, attribute, ignore_errors
    )
    for attribute, attribute_instances in instances_ompostering.items():
        assert not attribute_instances if assert_om_empty else attribute_instances

    provision_main._post_to_ompostering(
        ctx, instances_ompostering, attribute, ignore_errors
    )


@pytest.mark.parametrize(attribute_params, attribute_args)
def test_provision_attribute_ok(
    ompostering_ms_context,
    requests_mock,
    ubw_responses,
    attribute_name,
    ubw_url,
    om_url,
):
    _mock_urls(requests_mock, ubw_responses)
    _provision_no_cli(
        ompostering_ms_context,
        "72",
        attribute_name,
        ignore_errors=False,
        assert_ubw_empty=False,
        assert_om_empty=False,
    )


@pytest.mark.parametrize(attribute_params, attribute_args)
def test_provision_attribute_ubw_error_ignore(
    ompostering_ms_context,
    requests_mock,
    ubw_responses,
    attribute_name,
    ubw_url,
    om_url,
):
    _mock_urls(requests_mock, ubw_responses)
    requests_mock.get(ubw_url, exc=requests.exceptions.ConnectTimeout)
    _provision_no_cli(
        ompostering_ms_context,
        "72",
        attribute_name,
        ignore_errors=True,
        assert_ubw_empty=not attribute_name == "begrepsverdier",
        assert_om_empty=not attribute_name == "begrepsverdier",
    )


@pytest.mark.parametrize(attribute_params, attribute_args)
def test_provision_attribute_ubw_error(
    ompostering_ms_context,
    requests_mock,
    ubw_responses,
    attribute_name,
    ubw_url,
    om_url,
):
    _mock_urls(requests_mock, ubw_responses)
    requests_mock.get(ubw_url, exc=requests.exceptions.ConnectTimeout)
    with pytest.raises(Exception):
        _provision_no_cli(
            ompostering_ms_context,
            "72",
            attribute_name,
            ignore_errors=False,
            assert_ubw_empty=False,
            assert_om_empty=False,
        )


@pytest.mark.parametrize(attribute_params, attribute_args)
def test_provision_attribute_om_error_ignore(
    ompostering_ms_context,
    requests_mock,
    ubw_responses,
    attribute_name,
    ubw_url,
    om_url,
):
    _mock_urls(requests_mock, ubw_responses)
    requests_mock.post(om_url, exc=requests.exceptions.ConnectTimeout)
    _provision_no_cli(
        ompostering_ms_context,
        "72",
        attribute_name,
        ignore_errors=True,
        assert_ubw_empty=False,
        assert_om_empty=False,
    )


def test_provision_brukere_skip_no_role_and_company(
    ompostering_ms_context, requests_mock, ubw_responses
):
    """Do not provision users without an active, relevant rolesAndCompanies."""

    ubw_responses["brukere"][1]["rolesAndCompanies"] = []
    _mock_urls(requests_mock, ubw_responses)
    _provision_no_cli(
        ompostering_ms_context,
        "72",
        "brukere",
        ignore_errors=False,
        assert_ubw_empty=True,
        assert_om_empty=True,
    )


def test_provision_brukere_skip_not_n(
    ompostering_ms_context, requests_mock, ubw_responses
):
    """Do not provision inactive users."""

    ubw_responses["brukere"][1]["userStatus"]["status"] = "Not N"
    _mock_urls(requests_mock, ubw_responses)
    _provision_no_cli(
        ompostering_ms_context,
        "72",
        "brukere",
        ignore_errors=False,
        assert_ubw_empty=True,
        assert_om_empty=True,
    )


def test_provision_brukere_skip_expired(
    ompostering_ms_context, requests_mock, ubw_responses
):
    """Do not provision users with an expired status."""

    ubw_responses["brukere"][1]["userStatus"]["dateTo"] = (
        datetime.datetime.now() - datetime.timedelta(days=1)
    ).isoformat()
    _mock_urls(requests_mock, ubw_responses)
    _provision_no_cli(
        ompostering_ms_context,
        "72",
        "brukere",
        ignore_errors=False,
        assert_ubw_empty=True,
        assert_om_empty=True,
    )


def test_provision_brukere_skip_not_activated(
    ompostering_ms_context, requests_mock, ubw_responses
):
    """Do not provision users with an unactivated status."""

    ubw_responses["brukere"][1]["userStatus"]["dateFrom"] = (
        datetime.datetime.now() + datetime.timedelta(days=1)
    ).isoformat()
    _mock_urls(requests_mock, ubw_responses)
    _provision_no_cli(
        ompostering_ms_context,
        "72",
        "brukere",
        ignore_errors=False,
        assert_ubw_empty=True,
        assert_om_empty=True,
    )


def test_provision_brukere_skip_expired_roles(
    ompostering_ms_context, requests_mock, ubw_responses
):
    """Do not provision users with only expired roles."""
    yesterday = datetime.datetime.now() - datetime.timedelta(days=1)

    for role in ubw_responses["brukere"][1]["rolesAndCompanies"]:
        role["roleConnectionValidTo"] = yesterday.isoformat()

    _mock_urls(requests_mock, ubw_responses)
    _provision_no_cli(
        ompostering_ms_context,
        "72",
        "brukere",
        ignore_errors=False,
        assert_ubw_empty=True,
        assert_om_empty=True,
    )


def test_provision_brukere_skip_no_activated_roles(
    ompostering_ms_context, requests_mock, ubw_responses
):
    """Do not provision users without any activated roles."""
    tomorrow = datetime.datetime.now() + datetime.timedelta(days=1)

    for role in ubw_responses["brukere"][1]["rolesAndCompanies"]:
        role["roleConnectionValidFrom"] = tomorrow.isoformat()

    _mock_urls(requests_mock, ubw_responses)
    _provision_no_cli(
        ompostering_ms_context,
        "72",
        "brukere",
        ignore_errors=False,
        assert_ubw_empty=True,
        assert_om_empty=True,
    )


def test_provision_brukere_skip_no_relevant_roles(
    ompostering_ms_context, requests_mock, ubw_responses
):
    """Do not provision users with no roles for this company."""
    for role in ubw_responses["brukere"][1]["rolesAndCompanies"]:
        role["companyId"] = "Doesn't exist"

    _mock_urls(requests_mock, ubw_responses)
    _provision_no_cli(
        ompostering_ms_context,
        "72",
        "brukere",
        ignore_errors=False,
        assert_ubw_empty=True,
        assert_om_empty=True,
    )


def test_provision_kontoplan(ompostering_ms_context, requests_mock, ubw_responses):
    """Test fetch ubw kontoplan and post to ompostering"""
    _mock_urls(requests_mock, ubw_responses)
    _provision_no_cli(
        ompostering_ms_context,
        "72",
        "kontoplan",
        ignore_errors=False,
        assert_ubw_empty=False,
        assert_om_empty=False,
    )


def test_provision_anlegg(ompostering_ms_context, requests_mock, ubw_responses):
    """Test fetch ubw anlegg and post to ompostering"""
    _mock_urls(requests_mock, ubw_responses)
    _provision_no_cli(
        ompostering_ms_context,
        "72",
        "anlegg",
        ignore_errors=False,
        assert_ubw_empty=False,
        assert_om_empty=False,
    )


def test_provision_koststed(ompostering_ms_context, requests_mock, ubw_responses):
    """Test fetch ubw koststed and post to ompostering"""
    _mock_urls(requests_mock, ubw_responses)
    _provision_no_cli(
        ompostering_ms_context,
        "72",
        "koststeder",
        ignore_errors=False,
        assert_ubw_empty=False,
        assert_om_empty=False,
    )


def test_provision_koststed_missing_ubw(
    ompostering_ms_context, requests_mock, ubw_responses
):
    """Test fetch ubw koststed and post to ompostering"""
    ubw_responses["koststeder"] = []
    _mock_urls(requests_mock, ubw_responses)
    _provision_no_cli(
        ompostering_ms_context,
        "72",
        "koststeder",
        ignore_errors=False,
        assert_ubw_empty=True,
        assert_om_empty=True,
    )


def test_provision_koststed_ubw_error(
    ompostering_ms_context, requests_mock, ubw_responses
):
    """Test fetch ubw koststed and post to ompostering"""
    del ubw_responses["koststeder"][0]["companyId"]
    _mock_urls(requests_mock, ubw_responses)
    _provision_no_cli(
        ompostering_ms_context,
        "72",
        "koststeder",
        ignore_errors=True,
        assert_ubw_empty=True,
        assert_om_empty=True,
    )


def test_provision_avgiftskoder(ompostering_ms_context, requests_mock, ubw_responses):
    """Test fetch ubw avgiftskoder and post to ompostering"""
    _mock_urls(requests_mock, ubw_responses)
    _provision_no_cli(
        ompostering_ms_context,
        "72",
        "avgiftskoder",
        ignore_errors=False,
        assert_ubw_empty=False,
        assert_om_empty=False,
    )


def test_is_yes():
    for s in ["yes", "YES", "Y", "y", "yEs", "YeS"]:
        assert provision_main._is_yes(s)
    for s in ["n", "anything", "NO", "nO", "asdfgh"]:
        assert not provision_main._is_yes(s)


@pytest.mark.parametrize("module_name", provision_main.ATTRIBUTE_MODULES.keys())
def test_main(ubw_responses, module_name, caplog, requests_mock):
    missing_fixtures = {"konteringsregler"}  # TODO: Create fixture and test
    config_path = str(
        pathlib.Path(__file__).parent.joinpath("fixtures", "testconfig.yaml")
    )
    if module_name in missing_fixtures:
        pytest.xfail(f"Missing fixtures for module '{module_name}'")
    testargs = [
        provision_main.__file__,
        f"--{module_name}",
        "-c",
        config_path,
        "--no-confirm",
        "72",
    ]
    with caplog.at_level(logging.DEBUG), unittest.mock.patch.object(
        sys, "argv", testargs
    ), pytest.raises(SystemExit) as exc:
        _mock_urls(requests_mock, ubw_responses)
        provision_main.main()
    assert exc.value.code == 0
    assert f"Posting {module_name} to Ompostering..." in caplog.text
    if module_name == "brukere":
        assert (
            "provision.brukere",
            logging.INFO,
            "Posting brukere with company_id=72, antall brukere: 1...",
        ) in caplog.record_tuples


def test_main_help():
    result = subprocess.run(
        (sys.executable, provision_main.__file__, "--help"),
        text=True,
        stdout=subprocess.PIPE,
        check=False,
    )
    assert result.returncode == 0
    assert result.stdout.startswith("usage: python -m provision")
