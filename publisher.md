Ompostering publisher(listener)
================================

**If this service is not running, no transactions from Ompostering will be sent
to SETRA.**

What does it do
---------------

`oracle_listener` *listens* to an oracle database and *publishes* messages to a
RabbitMQ queue when a change is noticed.

The RabbitMQ queue is being consumed by the `ompostering-listener` service
which sees that the routing key of the message matches the routing key for the
task calling the `ensure_transaksjon_to_SETRA` handler.

This handler fetches the batch we were notified about from the oracle database,
and posts it to the `/addtrans` endpoint of the SETRA-api.

Where is it running
-------------------

Accessible at ompostering-publisher.uio.no as root from gt-auth.uio.no

- Host: ompostering-publisher.uio.no
- Jump-host: gt-auth.uio.no

Typical pattern for connecting

```bash
ssh foo@saruman.uio.no
ssh foo-drift@gt-auth.uio.no
sudo -i
ssh ompostering-publisher
su - ompostering
```

Running and logging
-------------------

At the time of writing, oracle_listener does not log to file by default. To
force it to do, so we run the service like so:

`poetry run python ompostering_ms/oracle_listener.py >> ompostering.log &`

Note the `>>` to avoid overwriting old log files.

If the service is updated to log to file on its own, the service can simply be
run with

`poetry run python ompostering_ms/oracle_listener.py`

How to update
-------------

As user *ompostering* on *ompostering-publisher*:

- navigate to */home/ompostering/ompostering-ms*
- run `git pull`
- find the process running oracle_listener (`ps aux | grep oracle_listener`)
- kill it
- Rotate log files
- run `poetry run python ompostering_ms/oracle_listener.py >> ompostering.log &`

Why not in Openshift?
---------------------

The oracle database we need to listen to is not accessible from the Openshift cluster,
and thus the service can't run there. If at some point the Openshift cluster is
opened a bit more so that we can reach the database from there, this vm can be retired.
