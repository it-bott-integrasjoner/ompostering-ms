# syntax=docker/dockerfile:experimental
# Use same python version in pyproject.toml, Dockerfile and .gitlab-ci.yml
FROM harbor.uio.no/mirrors/docker.io/library/python:3.10-slim

LABEL no.uio.contact=bott-int-drift@usit.uio.no

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y git

# Build dep for cx_Oracle
WORKDIR /opt/oracle

RUN apt-get install -y wget unzip libaio1
RUN wget https://download.oracle.com/otn_software/linux/instantclient/instantclient-basiclite-linuxx64.zip && \
    unzip instantclient-basiclite-linuxx64.zip && \
    rm -f instantclient-basiclite-linuxx64.zip && \
    cd instantclient* && \
    rm -f *jdbc* *occi* *mysql* *jar uidrvci genezi adrci && \
    echo /opt/oracle/instantclient* > /etc/ld.so.conf.d/oracle-instantclient.conf && \
    ldconfig

WORKDIR /

# Build dep done

ARG PIP_DEFAULT_TIMEOUT=100
ARG PIP_DISABLE_PIP_VERSION_CHECK=1
ARG PIP_NO_CACHE_DIR=1
ARG POETRY_VIRTUALENVS_CREATE=false

COPY ssh_known_hosts /root/.ssh/known_hosts

RUN install -v -d -m7700 /var/log/ompostering-ms && \
    pip3 install poetry==1.5.1

WORKDIR /ompostering-ms
COPY pyproject.toml poetry.lock /ompostering-ms/
RUN poetry install --no-dev --no-interaction --no-ansi


COPY . /ompostering-ms/

RUN chmod -v 755 /ompostering-ms

# The unit test may fails locally even using 3.8.* with pyenv,
# Use line below to run the unittest via docker
# CMD cd /ompostering-ms && poetry run pytest
# run via terminal: docker build -t omp . && docker run -t omp

